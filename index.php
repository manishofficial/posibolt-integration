<?php

/*
Plugin Name: POSibolt Integration
Plugin URI:
Description: E-Commerce Integration with woocommerce
Author: Asif Amod
Version: 2.0
Author URI: 2beards.co
*/

/**
*  Activation Class
**/
if ( ! class_exists( 'WC_CPInstallCheck' ) ) {
  class WC_CPInstallCheck {
    static function install() {
      /**
      * Check if WooCommerce are active
      **/
      if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

        // Deactivate the plugin
        deactivate_plugins(__FILE__);

        // Throw an error in the wordpress admin console
        $error_message = __('This plugin requires WooCommerce plugin to be Installed & active!', 'woocommerce');
        die($error_message);
      }else{
        my_plugin_create_db(); //Table create function
      }
    }
  }
}

register_activation_hook( __FILE__, array('WC_CPInstallCheck', 'install') );

set_time_limit(300);

error_reporting(1);
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
@ini_set( 'display_errors', 1 );
@ini_set( 'log_errors', 'On' );
@ini_set( 'error_log', WP_PLUGIN_DIR . '/posibolt-integration/logs/php_error.log' );
// add_action('init', 'pos_error_log');
function pos_error_log($msg){
  $pluginlog = WP_PLUGIN_DIR . '/posibolt-integration/logs/php_error.log';
  $logDate = date('d-M-Y h:i:s A (e)');
  $message = "[ $logDate ] : ".$msg.PHP_EOL;
  error_log($message, 3, $pluginlog);
}

//Image sync cron

function img_sync(){
  $message = "Runned Image cron job at ".date("d-m-Y H:i")."[new]";
  error_log($message);

  $url = plugin_dir_url( __FILE__ ).'pages/cron_get_pro_img.php';
  error_log("Accessing file at url ".$url);

  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 300,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  if($err)
  {
    error_log("cURL Error: ".$err);
  }
  else
  {
    error_log("Image Cron completed");
  }
}

//All Data sync cron
function run_my_script(){
  $message = "Runned cron job at ".date("d-m-Y H:i")."[new]";
  error_log($message);

  $url = plugin_dir_url( __FILE__ ).'pages/get_category.php';
  error_log("Accessing file at url ".$url);

  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 300,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  if($err)
  {
    error_log("cURL Error: ".$err);
  }
  else
  {
    error_log("Cron completed");
  }
}

if ( ! file_exists( WP_PLUGIN_DIR . '/woocommerce/woocommerce.php' ) ) {
    // No WooCommerce installed, we don't need this.
    return;
}

// If the posibolt-integration plugin already exists in the conventionally installed
// directory, do nothing in order to avoid conflicts.
if ( file_exists( WP_PLUGIN_DIR . '/posibolt-integration/index.php' ) ) {
if ( str_replace('\\', "/", WP_PLUGIN_DIR . '/posibolt-integration/') !== str_replace('\\', "/",plugin_dir_path( __FILE__ )) ) {
      // posibolt-integration is already installed conventionally, exiting to avoid conflict.
      return;
  }
}

//API CODE
add_action( 'rest_api_init', 'my_register_route' );
function my_register_route() {
    register_rest_route( 'pos-woo','orderstatus', array(
            'methods' => 'GET',
            'callback' => 'custom_phrase',
        )
    );
}
function custom_phrase() {
  include_once 'pages/api/update_status.php';
    return rest_ensure_response( $api_response );
}
//END API

//Instant Order sync webhook
include_once "pages/webhooks/order_webhook.php";
// add_action('woocommerce_thankyou', 'send_order_pos');
//Instant Order status sync webhook
include_once "pages/webhooks/status_webhook.php";
add_action( 'woocommerce_order_status_changed', 'order_status_webhook', 10, 4 );
//Order Status change Send order to POS
add_action('woocommerce_order_status_processing', 'Status_change_order_sync');
// order status hooks for logs
add_action( 'woocommerce_order_status_pending',   'showLogsAccordingOrderStatus');
add_action( 'woocommerce_order_status_failed',    'showLogsAccordingOrderStatus');
add_action( 'woocommerce_order_status_on-hold',   'showLogsAccordingOrderStatus');
add_action( 'woocommerce_order_status_completed', 'showLogsAccordingOrderStatus');
add_action( 'woocommerce_order_status_refunded',  'showLogsAccordingOrderStatus');
add_action( 'woocommerce_order_status_cancelled', 'showLogsAccordingOrderStatus');

//Woocommerce Checkout hook
add_action( 'woocommerce_before_checkout_form', 'custom_checkout', 10 );

function add_new_intervals($schedules){
  // add weekly and monthly intervals
  $schedules['weekly'] = array(
      'interval' => 604800,
      'display' => __('Once Weekly')
  );

  $schedules['monthly'] = array(
      'interval' => 2635200,
      'display' => __('Once a month')
  );
  return $schedules;
}
add_filter( 'cron_schedules', 'add_new_intervals');

//add_filter('cron_schedules',array('WC_Settings_MyPlugin', 'run_my_script'));
//Data Cron Register
add_action('POS_data_daily', 'run_my_script');
add_action('POS_data_twicedaily_1','run_my_script');
add_action('POS_data_twicedaily_2', 'run_my_script');
add_action('POS_data_weekly', 'run_my_script');
add_action('POS_data_monthly', 'run_my_script');
//Image Cron Register
add_action('POS_Image_daily', 'img_sync');
add_action('POS_Image_twicedaily_1','img_sync');
add_action('POS_Image_twicedaily_2', 'img_sync');
add_action('POS_Image_weekly', 'img_sync');
add_action('POS_Image_monthly', 'img_sync');

// add_action( 'custom_job','run_my_script' );
// if ( ! wp_next_scheduled( 'custom_job' ) ) {
//   wp_schedule_event( time(), 'daily', 'custom_job' );
// }

if ( ! wp_next_scheduled( 'POS_Image_daily' ) && 
    ! wp_next_scheduled( 'POS_Image_twicedaily_1' ) &&
    ! wp_next_scheduled( 'POS_Image_twicedaily_2' ) &&
    ! wp_next_scheduled( 'POS_Image_weekly' ) &&
    ! wp_next_scheduled( 'POS_Image_monthly' )
    ) {
  wp_schedule_event( time(), 'weekly', 'POS_Image_weekly' );
}
add_action( 'POS_Image_weekly','img_sync' );

define( 'PLUGIN_DIR', dirname(__FILE__).'/' );
include "pages/settings.php";

add_filter( 'woocommerce_get_settings_pages', 'my_plugin_add_settings', 15 );

// New fields create
include "pages/add_fields.php";

//Create Table
function my_plugin_create_db() {
  global $wpdb;
  $charset_collate = $wpdb->get_charset_collate();
  $table_name = $wpdb->prefix . 'POS_cate_map';
  $pro_map_table_name = $wpdb->prefix . 'POS_product_map';
  $table_api = $wpdb->prefix . 'POS_woo_api';

  $sql = "CREATE TABLE $table_name (
    id int NOT NULL AUTO_INCREMENT,
    time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    Woo_cate_name varchar(255),
    Woo_cate_Id int,
    Woo_cate_parent_Id int,
    POS_cate_name varchar(255),
    POS_cate_Id int,
    POS_cate_parent_Id int,
    UNIQUE KEY id (id)
  ) $charset_collate;";
  $sql_pro_table = "CREATE TABLE $pro_map_table_name (
    id int NOT NULL AUTO_INCREMENT,
    time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    woo_pro_field varchar(255),
    POS_pro_field varchar(255),
    UNIQUE KEY id (id)
  ) $charset_collate;";
  $api_table = "CREATE TABLE $table_api (
    id int NOT NULL AUTO_INCREMENT,
    time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    token varchar(255),
    link varchar(255),
    UNIQUE KEY id (id)
  ) $charset_collate;";

  if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
    $wpdb->query($sql);
  }
  if($wpdb->get_var("SHOW TABLES LIKE '$pro_map_table_name'") != $pro_map_table_name) {
    $wpdb->query($sql_pro_table);
  }
  if($wpdb->get_var("SHOW TABLES LIKE '$table_api'") != $table_api) {
    $wpdb->query($api_table);
  }
}

//Create Setting links
function my_plugin_action_links( $links ) {
  $links = array_merge( array(
    '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=pos&section=first' ) ) . '">' . __( 'Settings', 'textdomain' ) . '</a>'
  ), $links );
  return $links;
}
add_action( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'my_plugin_action_links' );
//End Setting links

//DEACTIVATE PLUGIN
register_deactivation_hook( __FILE__, 'myplugin_deactivate' );
function myplugin_deactivate(){
   //delete plugins option here ex:
  delete_option('wc_settings_pos_tab_url');
  delete_option('wc_settings_pos_tab_username');
  delete_option('wc_settings_pos_tab_password');
  delete_option('wc_settings_pos_tab_posuser');
  delete_option('wc_settings_pos_tab_pospassword');
  delete_option('wc_settings_pos_tab_terminal');
}

//UNINSTALL PLUGIN
register_uninstall_hook(__FILE__, 'myplugin_uninstall');
function myplugin_uninstall(){
  global $wpdb;
  $table_name = $wpdb->prefix . 'POS_cate_map';
  $pro_map_table_name = $wpdb->prefix . 'POS_product_map';
  $table_api = $wpdb->prefix . 'POS_woo_api';
  $query_structure = "drop table if exists $table_name";
  $query_pro_map = "drop table if exists $pro_map_table_name";
  $query_api_table_rmov = "drop table if exists $table_api";
  $wpdb->query($query_structure);
  $wpdb->query($query_pro_map);
  $wpdb->query($query_api_table_rmov);

  wp_clear_scheduled_hook('custom_job');
  wp_clear_scheduled_hook('POS_img_sync');
}