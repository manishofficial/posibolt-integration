<?php

	$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
	require_once $root.'/wp-load.php';

	pos_error_log("============================================================");
	pos_error_log("PRODUCT IMAGES SYNC START");
	pos_error_log("============================================================");
	
	function products() {
	    return array_map('wc_get_product', get_posts(['post_type'=>'product','nopaging'=>true]));
	}

	$products = products();
	
	if (empty($products)) {
		echo json_encode('<div><h2 style="color: red;">Woocommerce not have products.</h2></div>');
	}else{
		$check_pos_pro = check_woo_have_pos_pro($products);
		if ($check_pos_pro == False) {
			echo json_encode('<div><h2 style="color: red;">Woocommerce not have POS product yet.</h2></div>');
		}else{
			$data = array();
			foreach ($products as $product) {
				// $productId = $product->get_id();
				array_push( $data, array( 'productId' => $product->get_id(), 'productType' => $product->get_type() ) );
			}
			echo json_encode($data) ;
		}
	}

	function check_woo_have_pos_pro($products){
		$pos_data = array();
		foreach ($products as $product) {
			$productId = $product->get_id();
			$producType = $product->get_type();
			$POS_sim_pro_id = get_post_meta( $productId , 'POS_product_id', true );
			if (!empty($POS_sim_pro_id)) {
				array_push( $pos_data, array('POS_sim_pro_id' => $POS_sim_pro_id) );
			}

			if ($producType=='variable') {
				$args = array(
				    'post_type'     => 'product_variation',
				    'post_status'   => array( 'private', 'publish' ),
				    'numberposts'   => -1,
				    'orderby'       => 'menu_order',
				    'order'         => 'asc',
				    'post_parent'   => $productId // get parent post-ID
				);
				$variations = get_posts( $args );

				foreach ( $variations as $variation ) {
				    $variation_ID = $variation->ID;
				    $POS_var_pro_id = get_post_meta( $variation_ID , 'POS_custom_field', true );
				    if (!empty($POS_var_pro_id)) {
						array_push( $pos_data, array('POS_var_pro_id' => $POS_var_pro_id) );
					}
				}
			}
		}
		if (empty($pos_data)) {
			return False;
		}else{
			return True;
		}
	}