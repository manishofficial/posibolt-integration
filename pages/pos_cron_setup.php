<?php

$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require_once( $root.'/wp-load.php' );

$Array = $_POST['data'];
if(empty($Array[1])) {
    echo "<div class='notice notice-success is-dismissible'>
		    <p>Please Select Cron First.</p>
		  </div>";
    die();
}

$syncType = $Array[0][1];
if ($syncType == 'DataSync') {
	$syncVar = "POS_data_";
}else{
	$syncVar = "POS_Image_";
}


function my_add_intervals($schedules) {
	// add a 'weekly' & 'monthly' interval
	$schedules['weekly'] = array(
		'interval' => 604800,
		'display' => __('Once Weekly')
	);
	$schedules['monthly'] = array(
		'interval' => 2635200,
		'display' => __('Once a month')
	);
	return $schedules;
}
//get cron Time
$crons  = _get_cron_array();
$events = array();
foreach ( $crons as $time => $cron ) {
	foreach ( $cron as $hook => $dings ) {
		foreach ( $dings as $sig => $data ) {
			# This is a prime candidate for a Crontrol_Event class but I'm not bothering currently.
				$events[ "$hook" ] = (object) array(
					'hook'     => $hook,
					'time'     => $time,
					'sig'      => $sig,
					'args'     => $data['args'],
					'schedule' => $data['schedule'],
					'interval' => isset( $data['interval'] ) ? $data['interval'] : null,
				);
			}
		}
	}
$daily_time = strtotime(date('d-M-Y G:i',$events[$syncVar."daily"]->time));
$time_twic1 = strtotime(date('d-M-Y G:i',$events[$syncVar."twicedaily_1"]->time));
$time_twic2 = strtotime(date('d-M-Y G:i',$events[$syncVar."twicedaily_2"]->time));
$time_weekly = strtotime(date('d-M-Y G:i',$events[$syncVar."weekly"]->time));
$time_monthly = strtotime(date('d-M-Y G:i',$events[$syncVar."monthly"]->time));

if($Array[1]=="daily"){
	if (wp_next_scheduled ( $syncVar.'twicedaily_1' )) {
			wp_clear_scheduled_hook($syncVar.'twicedaily_1');
	}
	if (wp_next_scheduled ( $syncVar.'twicedaily_2' )) {
			wp_clear_scheduled_hook($syncVar.'twicedaily_2');
	}
	if (wp_next_scheduled ( $syncVar.'weekly' )) {
			wp_clear_scheduled_hook($syncVar.'weekly');
	}
	if (wp_next_scheduled ( $syncVar.'monthly' )) {
			wp_clear_scheduled_hook($syncVar.'monthly');
	}
	$time_daily = $Array[2];
	//echo "<br>".$time_daily;
	$chk_time = strtotime(date('d-M-Y').$time_daily);
	if (! wp_next_scheduled ( $syncVar.'daily' )) {
		wp_schedule_event( strtotime($time_daily), 'daily', $syncVar.'daily');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	elseif ($daily_time != $chk_time) {
		wp_clear_scheduled_hook($syncVar.'daily');
		wp_schedule_event( strtotime($time_daily), 'daily', $syncVar.'daily');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	else{
		echo "<div class='notice notice-error is-dismissible'>
			    <p>Schedule already exists.</p>
			  </div>";
	}
}

if($Array[1]=="twicedaily"){
	$time_twice_one = $Array[2];
	$time_twice_two = $Array[3];
	$chk_time_twice1 = strtotime(date('d-M-Y').$time_twice_one);
	$chk_time_twice2 = strtotime(date('d-M-Y').$time_twice_two);
	$chk_time_twice_one = strtotime(date('d-M-Y').$time_twice_one);
	$chk_time_twice_two = strtotime(date('d-M-Y').$time_twice_two);
	if (wp_next_scheduled ( $syncVar.'daily' )) {
			wp_clear_scheduled_hook($syncVar.'daily');
	}
	if (wp_next_scheduled ( $syncVar.'weekly' )) {
			wp_clear_scheduled_hook($syncVar.'weekly');
	}
	if (wp_next_scheduled ( $syncVar.'monthly' )) {
			wp_clear_scheduled_hook($syncVar.'monthly');
	}

	if (! wp_next_scheduled ( $syncVar.'twicedaily_1' )) {
		wp_schedule_event( strtotime($time_twice_one), "daily", $syncVar.'twicedaily_1');

		if (! wp_next_scheduled ( $syncVar.'twicedaily_2' )) {
			wp_schedule_event( strtotime($time_twice_two), "daily", $syncVar.'twicedaily_2');
			echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
		}
	}
	elseif ($time_twic1 != $chk_time_twice1) {
		wp_clear_scheduled_hook($syncVar.'twicedaily_1');
		wp_schedule_event( strtotime($time_twice_one), "daily", $syncVar.'twicedaily_1');

		if ($time_twic2 != $chk_time_twice2) {
			wp_clear_scheduled_hook($syncVar.'twicedaily_2');
			wp_schedule_event( strtotime($time_twice_two), "daily", $syncVar.'twicedaily_2');
		}
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	else{
		echo "<div class='notice notice-error is-dismissible'>
			    <p>Schedule already exists.</p>
			  </div>";
	}
}

if($Array[1]=="weekly"){

	$day_weekly = $Array[2];
	$chk_time_weekly = strtotime(date('d-M-Y').$day_weekly);
	if (wp_next_scheduled ( $syncVar.'daily' )) {
			wp_clear_scheduled_hook($syncVar.'daily');
	}
	if (wp_next_scheduled ( $syncVar.'twicedaily_1' )) {
			wp_clear_scheduled_hook($syncVar.'twicedaily_1');
	}
	if (wp_next_scheduled ( $syncVar.'twicedaily_2' )) {
			wp_clear_scheduled_hook($syncVar.'twicedaily_2');
	}
	if (wp_next_scheduled ( $syncVar.'monthly' )) {
			wp_clear_scheduled_hook($syncVar.'monthly');
	}
	if (! wp_next_scheduled ( $syncVar.'weekly' )) {

		$times = explode(":", $Array[3]);
		$hours = $times[0];
		$minutes = $times[1];
		$day = $Array[2];
		$weeklyDate = strtotime("next ".$day);
		$weeklyDate += ($hours*3600)+($minutes*60);
		$chk_time = strtotime(date('d-M-Y').$weeklyDate);

		wp_schedule_event( $weeklyDate, $Array[1], $syncVar.'weekly');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	 }
	 elseif ($weeklyDate != $chk_time) {
		wp_clear_scheduled_hook($syncVar.'weekly');
		wp_schedule_event( $weeklyDate, $Array[1], $syncVar.'weekly');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	else{
		echo "<div class='notice notice-error is-dismissible'>
			    <p>Schedule already exists.</p>
			  </div>";
	}
}

if($Array[1]=="monthly"){
	if (wp_next_scheduled ( $syncVar.'daily' )) {
			wp_clear_scheduled_hook($syncVar.'daily');
	}
	if (wp_next_scheduled ( $syncVar.'twicedaily_1' )) {
			wp_clear_scheduled_hook($syncVar.'twicedaily_1');
	}
	if (wp_next_scheduled ( $syncVar.'twicedaily_2' )) {
			wp_clear_scheduled_hook($syncVar.'twicedaily_2');
	}
	if (wp_next_scheduled ( $syncVar.'weekly' )) {
			wp_clear_scheduled_hook($syncVar.'weekly');
	}
	$day_monthly = $Array[2];
	$chk_time_monthly = strtotime(date('d-M-Y').$day_monthly);
	if (! wp_next_scheduled ( $syncVar.'monthly' )) {
		wp_schedule_event( strtotime('next '.$day_monthly), $Array[1], $syncVar.'monthly');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	else{
		echo "<div class='notice notice-error is-dismissible'>
			    <p>Schedule already exists.</p>
			  </div>";
	}
}
