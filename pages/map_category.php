<?php
$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require_once( $root.'/wp-load.php' );

global $wpdb;

$map_pro_table = $wpdb->prefix . 'POS_product_map';

$map_data = array();
parse_str($_POST['data'], $map_data);

foreach ($map_data as $key => $value) {
    $check_exists_keys = $wpdb->get_results("SELECT * FROM $map_pro_table WHERE woo_pro_field='".$key."'");

    if (empty($check_exists_keys)) {
        $wpdb->insert(
            $map_pro_table,array(
                'woo_pro_field' => $key,
                'POS_pro_field' => $value
            ));
    }else{
        $where = array('woo_pro_field' => $key);
        $wpdb->update(
            $map_pro_table,array(
                'POS_pro_field' => $value
            ),$where
        );
    }
}

set_time_limit(300);
include_once( dirname( __FILE__ ) . '/get_token.php' );
$token_id = token_genrate();
//Get loading GIF
$gif = dirname(plugin_dir_url( __FILE__ ));
$gif_link =  $gif.'/loadingdata.gif';

if($token_id==NULL){
    echo "<div class='notice notice-error is-dismissible'>
			<p>Something went wrong.<a href='".admin_url()."/admin.php?page=wc-settings&tab=pos&section=first'> Click here</a> to check POS connection settings..</p>
		  </div>";
    die();
}

$table_name = $wpdb->prefix . 'POS_cate_map';

//GET WOOCOMMERCE CATEGORIES
$cat_args = array(
    'orderby'    => 'name',
    'order'      => 'asc',
    'hide_empty' => false,
);
 
$product_categories = get_terms( 'product_cat', $cat_args );
 
if( !empty($product_categories) ){
    ?>
    <div id="results">
                <!-- All data will display here  -->
        </div>
        <div class="Load" id="Load" style="display:none;">
            <p>Please do not refresh this window or click the Back button on your browser.</p>
            <img src="<?php echo $gif_link ?>" height="40px" width="40px"><br>
        </div>
        <div class="status" id="status" style="display:none;">
            <h2 style="color: #5cb85c;">Data Sync Complete</h2>
        </div>
        <div class="sync_error" id="sync_error" style="display:none;">
            <h2 style="color: red;">Something went wrong please try again later</h2>
        </div>
        <div id="progress_bar_back" style="display:none;">
          <div id="Progress_Status" class="progress" style=""> 
            <div id="myprogressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">0%</div> 
          </div>
        </div>
        
    <div class="myDIV2" id="myDIV2" style="display:block;">
        <center><h2><u>MAP CATEGORIES</u></h2></center>
    </div>
<div class="Sync_POS">
        <div class="woo_cate">
            <h2>WOOCOMMERCE EXISTSING CATEGORIES</h2>
            <table border="2">
                <tr>
                    <th>CATEGORY ID</th>
                    <th>CATEGORY NAME</th>
                    <th>CATEGORY PARENT ID</th>
                </tr>
                <?php foreach ($product_categories as $key => $category) { ?>
                <tr>
                    <td>
                        <?php echo $category->term_id; ?>
                    </td>
                    <td>
                        <?php echo $category->name; ?>
                    </td>
                    <td>
                        <?php echo $category->parent; ?>
                    </td>
                </tr>
                <?php 

                } ?>
            </table>
        </div>
    <?php
}
    // get default url form wp options table
    $api_url = str_replace(' ', '%20',get_option('wc_settings_pos_tab_url'));
    // get current date and time
    $current_time = strtotime(date('Y-m-d H:i:s'));

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/productcategorylist?updatedSince=".$current_time."&webstoreFeaturedOnly=true&access_token=".$token_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => array(),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err;
            pos_error_log("cURL Error #:" . $err);
            pos_error_log("============================================================");
            pos_error_log("GET CATEGORIES");
            pos_error_log("============================================================");
        }
        else {
            $get_all_category = json_decode($response);
            // echo "<pre>"; print_r($get_all_category); echo "</pre>";
            $productCount = getProductCount($current_time,$api_url,$token_id);
?>      
    <div class="POS_sync_cate">
        <h2>POS CATEGORIES</h2>
        <form name="map_cate" method="post">
            <table border="1">
                <tr>
                    <th>Product Category Id</th>
                    <th>Name</th>
                    <th>Parent Category Id</th>
                    <th>Select woo category for map</th>
                </tr>
                <?php
                $i=0;
                foreach ($get_all_category as $category) { 
                    $check_exists_cate = $wpdb->get_results("SELECT * FROM $table_name WHERE POS_cate_Id=".$category->productCategoryId);
                    ?>
                    <tr>
                        <td>
                            <input type="text" name="id[<?php echo $i ?>]" value="<?php echo $category->productCategoryId; ?>"id="id" readonly>
                        </td>
                        <td>
                            <input type="text" name="name[<?php echo $i ?>]" value="<?php echo $category->name; ?>" id="name" readonly>
                        </td>
                        <td>
                            <input type="text" name="parent[<?php echo $i ?>]" value="<?php echo $category->parentCategoryId; ?>" id="parent" readonly>
                        </td>
                        <td>
                            <select name="map_data[<?php echo $i ?>]">
                                <option>Select For Map</option>
                                <?php foreach ($product_categories as $key => $category) { ?>
                                <option value="<?php echo $category->term_id; ?>|<?php echo $category->name; ?>|<?php echo $category->parent; ?>" <?php if($check_exists_cate[0]->Woo_cate_name == $category->name) echo"selected"; ?>><?php echo $category->name; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                <?php $i++; } ?>
                <tr>
                    <td colspan="4">
                        <center>
                            <input type="submit" name="get_data" style="border: none; width:100%;cursor: pointer;" data-loading-text="Loading..." value="Continue Sync" class="insert" id="insert">
                        </center>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<?php   }

function getProductCount($current_time,$api_url,$token_id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/productmaster/productlist?updatedSince=".$current_time."&webstoreFeaturedOnly=true&access_token=".$token_id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      // echo "cURL Error #:" . $err;
      pos_error_log("cURL Error #:" . $err);
      pos_error_log("============================================================");
      pos_error_log("GET PRODUCT COUNT");
      pos_error_log("============================================================");
    } else {
        return count( json_decode($response) );
    }
}
        ?>
        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="<?php echo $gif; ?>/css/progressbar.css">

<script type="text/javascript">
    var offset = 0;
    var limit = 20;
    var width = 0;
    var element = document.getElementById("myprogressBar");
    var identity;
    var firstIndentity;
    var lastIdentity;
    var productCount = "<?php echo $productCount; ?>";
    var perApiLimit = 100/( productCount/20) ;
    var apiHitCount = 1;
    var newApiLimit = 0;
    var oldoffset = limit;
    // clearInterval(identity); 

    jQuery("form").on("submit", function (e) {
        e.preventDefault();
        var formData = jQuery('form').serialize();

        jQuery('input[type="submit"]').attr('disabled','disabled');
        jQuery(".myDIV2").css("display", "none");
        jQuery(".Sync_POS").css("display", "none");
        jQuery('#myDIV2').hide();
        jQuery('#Load').show();
        jQuery('#progress_bar_back').show();
        getPosData(formData);
    });

    function getPosData(formData){
        
        if(offset == 0){
            firstIndentity = setInterval(firstTime, 2000);
        }
        var formData = formData;
        jQuery.ajax({
            type: "POST",
            url: "<?php echo plugin_dir_url( __FILE__ ).'get_category.php'; ?>",
            data: { 
                    data  : formData,
                    offsetPos: offset,
                    limitPos : limit,
                    productCount : productCount
                  },
            success: function(data) {
                // jQuery("#results").html(data);
                offset = offset+limit;
                apiHitCount = apiHitCount+1;
                if(offset > 0){
                    clearInterval(firstIndentity);
                }
                identity = setInterval(scene, 1000);

                if(offset < productCount){
                    getPosData(formData);
                }else{
                    offset = 0;
                    limit = 20;
                    lastIdentity = setInterval(fullWidthProgressBar, 1000);
                    
                }
            },
            error: function(data){
                // jQuery("#results").html(data);
                clearInterval(firstIndentity);
                clearInterval(identity);
                width = 0;
                offset = 0;
                limit = 20;
                jQuery('#progress_bar_back').hide();
                jQuery('#Load').hide();
                jQuery('#sync_error').show();
                // setTimeout(function() {
                //     $('#sync_error').fadeOut();
                // }, 5000);
                jQuery('#cate_map').attr('style', 'display:none');
                jQuery("#sync_dis").prop("disabled", true);
                setTimeout(function() {
                   jQuery("#sync_dis").prop("disabled", false);
                }, 600000);
                jQuery('input[type="submit"]').removeAttr('disabled');
                jQuery('#cate_map')[0] && jQuery('#cate_map')[0].reset();
            }
        });
    }
    function scene() {
        if(width < newApiLimit && width < 100 ){
            width = width+0.2;
            element.style.width = width + '%';
            var showPercent = width * 1;
            element.innerHTML = Math.round(showPercent)+'%';
        }else{
            newApiLimit = perApiLimit*apiHitCount
        }
    }
    function firstTime(){
        width = width+0.2;
        var widthLength = perApiLimit;
        if (width<=widthLength) {
            element.style.width = width + '%'; 
            var showPercent = width * 1;
            element.innerHTML = Math.round(showPercent)+'%';
        }else{
            clearInterval(firstIndentity);
        }  
    }

    function fullWidthProgressBar(){
         if(width < 95) { 
          width = width+5; 
          element.style.width = width + '%'; 
          var showPercent = width * 1;
          element.innerHTML = Math.round(showPercent)+'%'; 
        }else{
            clearInterval(lastIdentity); 
            width = 100; 
            element.style.width = width + '%'; 
            var showPercent = width * 1;
            element.innerHTML = Math.round(showPercent)+'%';
            jQuery('#Load').hide();
            jQuery('#status').show();
            // setTimeout(function() {
            //     $('#status').fadeOut();
            // }, 5000);
            jQuery('#cate_map').attr('style', 'display:none');
            jQuery('.ajaxsync').attr('style', 'display:block');
            jQuery("#sync_dis").attr('style', 'background-color:#b0b6bb');
            jQuery("#sync_dis").prop("disabled", true);
            setTimeout(function() {
                jQuery("#sync_dis").attr('style', 'background-color:#3a7aab');
                jQuery("#sync_dis").prop("disabled", false);
            }, 600000);
            jQuery('input[type="submit"]').removeAttr('disabled');
            jQuery('#cate_map')[0] && jQuery('#cate_map')[0].reset();
        }
    }
</script>