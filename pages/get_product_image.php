<?php
	$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
	require_once $root.'/wp-load.php';

	
	$productId = $_REQUEST['id'];
	$productType = $_REQUEST['proType'];
	echo $length = $_REQUEST['length'];
	echo $count = $_REQUEST['count'];

	
	include_once( dirname( __FILE__ ) . '/get_token.php' );
	$token_id = token_genrate();
	$api_url = str_replace(' ', '%20',get_option('wc_settings_pos_tab_url'));

	$POS_sim_pro_id = get_post_meta( $productId , 'POS_product_id', true );
	if (!empty($POS_sim_pro_id)) {
		checkExistOrDeleteImage($productId);
		$data = getPosProductImage($api_url,$POS_sim_pro_id,$token_id);
		if($data != '' || $data != NULL){
		    $imageAttachId = uploadPosProductImage($data,$root);
		    update_post_meta( $productId, '_thumbnail_id', $imageAttachId );
		    echo "<br>productId : ".$productId." Image Found";
		}else{
			echo "<br>productId : ".$productId." Image Not Found";
		}
	}

	if ($productType=='variable') {
		$args = array(
		    'post_type'     => 'product_variation',
		    'post_status'   => array( 'private', 'publish' ),
		    'numberposts'   => -1,
		    'orderby'       => 'menu_order',
		    'order'         => 'asc',
		    'post_parent'   => $productId // get parent post-ID
		);
		$variations = get_posts( $args );

		foreach ( $variations as $variation ) {
		    $variation_ID = $variation->ID;
		    $POS_var_pro_id = get_post_meta( $variation_ID , 'POS_custom_field', true );
		    if (!empty($POS_var_pro_id)) {
				checkExistOrDeleteImage($variation_ID);
				$data = getPosProductImage($api_url,$POS_var_pro_id,$token_id);
				if($data != '' || $data != NULL){
				    $imageAttachId = uploadPosProductImage($data,$root);
				    update_post_meta( $variation_ID, '_thumbnail_id', $imageAttachId );
				    echo "<br>variation_ID : ".$variation_ID." Image Found";
				}
			}
		}
	}

	function checkExistOrDeleteImage($product_id){
	    $img_data = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );
	    $img_file = $img_data[0];
	    $attachment_id = attachment_url_to_postid($img_file);
	    wp_delete_attachment( $attachment_id );
	}

	function getPosProductImage($api_url, $productId, $token_id){
	  $curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/productmaster/productimage/".$productId."?updatedSince=1535889329&webstoreFeaturedOnly=true&imageType=WBI&access_token=".$token_id,
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "GET",
	      CURLOPT_POSTFIELDS => "",
	      CURLOPT_HTTPHEADER => array(),
	    ));

	    $response = curl_exec($curl);
	    $err = curl_error($curl);

	    curl_close($curl);
	    if ($err) {
	      // echo "cURL Error #:" . $err;
	      pos_error_log("cURL Error #:" . $err);
	      pos_error_log("============================================================");
	      pos_error_log("GET PRODUCT IMAGES");
	      pos_error_log("============================================================");
	    } else {
	      return $response;
	    }
	}

	function uploadPosProductImage($response,$root){

	  $get_images = json_decode($response);
	  	//Upload and Get Variation Image
	    $postId = "";
	    $image = $get_images->data;
	    $directory = "/".date('Y')."/".date('m')."/";
	    $wp_upload_dir = wp_upload_dir();
	    $data = base64_decode($image);
	    $filename = $get_images->fileName;
	    //$fileurl = $wp_upload_dir['url'] . '/' . basename( $filename );
	    $fileurl = $root."/wp-content/uploads".$directory.$filename;

	    $filetype = wp_check_filetype( basename( $fileurl), null );

	    file_put_contents($fileurl, $data);

	    $attachment = array(
	        'guid' => $wp_upload_dir['url'] . '/' . basename( $fileurl ),
	        'post_mime_type' => $filetype['type'],
	        'post_title' => preg_replace('/\.[^.]+$/', '', basename($fileurl)),
	        'post_content' => '',
	        'post_status' => 'inherit'
	    );
	    //  print_r($attachment);

	    $attach_id = wp_insert_attachment( $attachment, $fileurl ,$postId);

	    // Generate the metadata for the attachment, and update the database record.
	    $attach_data = wp_generate_attachment_metadata( $attach_id, $fileurl );
	    wp_update_attachment_metadata( $attach_id, $attach_data );

	    return $attach_id;
	}

	if ( $count >= $length ) {
		pos_error_log("============================================================");
		pos_error_log("PRODUCT IMAGES SYNC END");
		pos_error_log("============================================================");
	}
	