<?php
$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require_once( $root . '/wp-load.php' );
function token_genrate(){
  $link = get_option('wc_settings_pos_tab_url')."/AdempiereService/oauth/token?";
  $username = str_replace(' ', '%20',get_option('wc_settings_pos_tab_username'));
  $password = str_replace(' ', '%20',get_option('wc_settings_pos_tab_password'));
  $pos_user = str_replace(' ', '%20',get_option('wc_settings_pos_tab_posuser'));
  $pos_pwd  = str_replace(' ', '%20',get_option('wc_settings_pos_tab_pospassword'));
  $terminal = str_replace(' ', '%20',get_option('wc_settings_pos_tab_terminal'));
  if ($link!='' && $username!='' && $password!='' && $pos_user!='' && $pos_pwd!='' && $terminal!='') {
    //GET ACCESS TOKEN
    $url = $link."username=".$pos_user."&password=".$pos_pwd."&terminal=".$terminal."&grant_type=password";

    $auth = base64_encode($username.":".$password);
    $get_data = '';
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic ".$auth
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      pos_error_log("cURL Error #:" . $err);
      pos_error_log("============================================================");
      pos_error_log("TOKEN IS NOT GENRATED");
      pos_error_log("============================================================");
    } else {
      $get_data = json_decode($response);
    }
    return $get_data->access_token;
  }
}