<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Display the custom text field
 * @since 1.0.0
 */
function cfwc_create_custom_field() {
    $args = array(
        'id'                => 'POS_product_id',
        'label'             => __( 'POS Products Id', 'cfwc' ),
        'class'             => 'POS-product-id',
        'desc_tip'          => true,
        'description'       => __( 'HERE WILL BE SHOW POS PRODUCT ID. THIS IS NOT MANDATORY.', 'ctwc' ),
        'custom_attributes' =>  array('readonly' => 'readonly'),
    );
    woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'cfwc_create_custom_field' );

/**
 * Save the custom field
 * @since 1.0.0
 */
function cfwc_save_custom_field( $post_id ) {
    $product = wc_get_product( $post_id );
    $title = isset( $_POST['POS_product_id'] ) ? $_POST['POS_product_id'] : '';
    $product->update_meta_data( 'POS_product_id', sanitize_text_field( $title ) );
    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field' );



// 1. Add custom field input @ Product Data > Variations > Single Variation
  
add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations', 10, 3 ); 
 
function bbloomer_add_custom_field_to_variations( $loop, $variation_data, $variation ) {
woocommerce_wp_text_input( array( 
              'id' => 'POS_custom_field', 
              'class' => 'POS_custom_field', 
              'label' => __( 'POS Product variation ID', 'woocommerce' ),
              'value' => get_post_meta( $variation->ID, 'POS_custom_field', true ),
              'custom_attributes' =>  array('readonly' => 'readonly'),
              ) 
);      
}
// -----------------------------------------
// 2. Store custom field value into variation data
  
add_filter( 'woocommerce_available_variation', 'bbloomer_add_custom_field_variation_data' );
 
function bbloomer_add_custom_field_variation_data( $variations ) {
    $variations['POS_custom_field'] = get_post_meta( $variations[ 'variation_id' ], 'POS_custom_field', true ) ;
    return $variations;
}

//Add user field
   add_action( 'show_user_profile', 'extra_user_profile_fields' );
   add_action( 'edit_user_profile', 'extra_user_profile_fields' );

   function extra_user_profile_fields( $user ) {
?>
       <h3 style="display: none;"><?php _e("Extra profile information", "blank"); ?></h3>

       <table class="form-table" style="/*display: none;*/">
       <tr>
           <th><label for="POS_customer_code"><?php _e("POS Customer Code"); ?></label></th>
           <td>
               <input type="text" name="POS_customer_code" id="POS_customer_code" value="<?php echo esc_attr( get_the_author_meta( 'POS_customer_code', $user->ID ) ); ?>" class="regular-text" disabled/><br />
               <span class="description"><?php _e("Please enter your POS customer code."); ?></span>
           </td>
       </tr>
       </table>
<?php
   }
function save_extra_user_profile_fields( $user_id ) {
   if ( !current_user_can( 'edit_user', $user_id ) ) {
       return false;
   }
}
add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );


?>
