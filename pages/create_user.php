<?php
$api_url = str_replace(' ', '%20',get_option('wc_settings_pos_tab_url'));
//GET CUSTOMERS
$args = array(
	'role'         => 'customer',
	// 'meta_key'     => 'POS_customer_code',
	'meta_query'   => array(
		array(
              "key"     => "POS_customer_code",
              "compare" => "NOT EXISTS",
        ),
	),
 ); 
$customers = get_users( $args );
$userArray = array();
foreach ($customers as $customer ) {
	$users_info = get_user_meta ( $customer->ID);
	$customerId = $customer->ID;
	$POScustCode = $customerId."-woo-".get_bloginfo( 'name' );
	$array = array(
					"customerCode" 	=> $POScustCode,
					"name" 			=> $users_info["first_name"][0],
					"name2" 		=> $users_info["last_name"][0],
					"address1" 		=> $users_info["shipping_address_1"][0],
					"address2" 		=> $users_info["shipping_address_2"][0],
					"city" 			=> $users_info["shipping_city"][0],
					"mobile"		=> $users_info["billing_phone"][0],
					"country" 		=> WC()->countries->countries[ $users_info["billing_country"][0] ],
					"postalCode" 	=> $users_info["shipping_postcode"][0],
					"email" 		=> $users_info["billing_email"][0],
					"active" 		=> true,
					"action" 		=> "create"
                );
	array_push($userArray, $array);		
}

if (!empty($userArray)) {
	foreach ($userArray as $user) {
	    $finalJson = json_encode($user);

	//CREATE CUSTOMERS
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/customermaster?access_token=".$token_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $finalJson,
		  CURLOPT_HTTPHEADER => array(
		    "Accept: application/json",
		    "Authorization: Bearer ".$token_id,
		    "Content-Type: application/json",
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  	// echo "cURL Error #:" . $err;
		  	pos_error_log("============================================================");
	        pos_error_log("CUSTOMERS SEND TO POS FAILD");
	        pos_error_log("============================================================");
	        pos_error_log("cURL Error #:" . $err);
		} else {
			$response = (array)json_decode($response);
			if ($response['responseCode'] != '400') {
				update_user_meta( $customerId, "POS_customer_code", $user['customerCode'] );
			}
		}
	}
}