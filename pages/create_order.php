<?php
//CREATE ORDER FROM WOOCOMMERCE TO POS
$Order_Array = array();
$orders = wc_get_orders( array() );
foreach ($orders as $order) {

  if ($order->get_status()=='processing') {

    $user_pos_id = get_user_meta( $order->user_id, 'POS_customer_code', true); //USER POS ID
    $get_user_data=get_userdata($order->user_id);
    $get_user_role=$get_user_data->roles;

    if ($get_user_role[0]=='customer') {
      if ($user_pos_id == NULL) {
        $user_data = array(
              "customerCode"  => $order->user_id."-woo-".get_bloginfo( 'name' ),
              "name"      => $order->get_billing_first_name(),
              "address1"    => $order->get_billing_address_1(),
              "address2"    => $order->get_billing_address_2(),
              "city"      => $order->get_billing_city(),
              "country"     => WC()->countries->countries[ $order->get_billing_country() ],
              "active"    => true,
              "action"    => "create"
                    );
        $finalJson = json_encode($user_data);

        $customerCode = $order->user_id."-woo-".get_bloginfo( 'name' );

      //CREATE WOOCOMMERCE CUSTOMERS TO POS
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/customermaster?access_token=".$token_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $finalJson,
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Authorization: Bearer ".$token_id,
            "Content-Type: application/json",
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
              pos_error_log("cURL Error #:" . $err);
              pos_error_log("============================================================");
              pos_error_log("CUSTOMERS SEND TO POS FAILD");
              pos_error_log("============================================================");
        } else {
              pos_error_log($response);
              pos_error_log("============================================================");
              pos_error_log("CUSTOMERS SEND STATUS");
              pos_error_log("============================================================");

              update_user_meta( $order->user_id, "POS_customer_code", $order->user_id."-woo-".get_bloginfo( 'name' ) );
              $user_pos_id = get_user_meta( $order->user_id, 'POS_customer_code', true);
        }
      }else{
          $customerCode = $user_pos_id;
        }
    }else{
      if ($user_pos_id == NULL) {
        //CREATE GUEST AS WOOCOMMERE CUSTOMER AND SYNC TO POS
          if ($get_user_role[0]=='administrator') {
            $user_id = $order->user_id;
            update_user_meta( $user_id, "POS_customer_code", $user_id."-woo-".get_bloginfo( 'name' ) );
            $customerCode = $user_id."-woo-".get_bloginfo( 'name' );
          }else{
            $user_id = wp_create_user( $order->get_billing_first_name(), wp_generate_password(), $order->get_billing_email() );
            $user_id_role = new WP_User($user_id);
            $user_id_role->set_role('customer');
            update_user_meta( $user_id, "billing_first_name", $order->get_billing_first_name() );
            update_user_meta( $user_id, "billing_last_name", $order->get_billing_last_name() );
            update_user_meta( $user_id, "billing_company", $order->get_billing_company() );
            update_user_meta( $user_id, "billing_address_1", $order->get_billing_address_1() );
            update_user_meta( $user_id, "billing_address_2", $order->get_billing_address_2() );
            update_user_meta( $user_id, "billing_city", $order->get_billing_city() );
            update_user_meta( $user_id, "billing_postcode", $order->get_billing_postcode() );
            update_user_meta( $user_id, "billing_country", $order->get_billing_country() );
            update_user_meta( $user_id, "billing_state", $order->get_billing_state() );
            update_user_meta( $user_id, "billing_email", $order->get_billing_email() );
            update_user_meta( $user_id, "billing_phone", $order->get_billing_phone() );
            // Shipping Details
            update_user_meta( $user_id, "shipping_first_name", $order->get_shipping_first_name() );
            update_user_meta( $user_id, "shipping_last_name", $order->get_shipping_last_name() );
            update_user_meta( $user_id, "shipping_company", $order->get_shipping_company() );
            update_user_meta( $user_id, "shipping_address_1", $order->get_shipping_address_1() );
            update_user_meta( $user_id, "shipping_address_2", $order->get_shipping_address_2() );
            update_user_meta( $user_id, "shipping_city", $order->get_shipping_city() );
            update_user_meta( $user_id, "shipping_postcode", $order->get_shipping_postcode() );
            update_user_meta( $user_id, "shipping_country", $order->get_shipping_country() );
            update_user_meta( $user_id, "shipping_state", $order->get_shipping_state() );
            update_user_meta( $user_id, "POS_customer_code", $user_id."-woo-".get_bloginfo( 'name' ) );
            update_post_meta( $order->get_id() , '_customer_user', $user_id );

            $customerCode = $user_id."-woo-".get_bloginfo( 'name' );
          }

        $user_data = array(
              "customerCode"  => $customerCode,
              "name"      => $order->get_billing_first_name(),
              "address1"    => $order->get_billing_address_1(),
              "address2"    => $order->get_billing_address_2(),
              "city"      => $order->get_billing_city(),
              "country"     => WC()->countries->countries[ $order->get_billing_country() ],
              "active"    => true,
              "action"    => "create"
                    );
        $finalJson = json_encode($user_data);

      //CREATE CUSTOMERS
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/customermaster?access_token=".$token_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $finalJson,
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Authorization: Bearer ".$token_id,
            "Content-Type: application/json",
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
              pos_error_log("cURL Error #:" . $err);
              pos_error_log("============================================================");
              pos_error_log("CUSTOMERS SEND TO POS FAILD");
              pos_error_log("============================================================");
        } else {
              pos_error_log($response);
              pos_error_log("============================================================");
              pos_error_log("CUSTOMERS SEND STATUS");
              pos_error_log("============================================================");
        }
      }
    }
  }else{
    die();
  }
      //ORDER DETAILS AND SYNC
      $orderID = $order->get_id();
      $orderDate = date( "d-m-Y", strtotime($order->get_date_created()) );
      $paymenTitle = $order->get_payment_method_title();

      $items = $order->get_items();
      if ($paymenTitle=="Direct bank transfer") {
        $pay_type = "EFT";
      }elseif ($paymenTitle=="Cash on delivery") {
        $pay_type = "cash";
      }else{
        $pay_type = "EFT";
      }

      $productsArray = array(); //define array to store products
      foreach ( $items as $item ) {
        $item->get_name();
        $product_id = $item->get_product_id();
        $quantity = $item->get_quantity();
        $total = $item->get_total();
        $my_meta = get_post_meta( $product_id, 'POS_product_id', true );

        array_push($productsArray,array("productId"=>$my_meta, "qty"=>$quantity, "price"=> $total)); 
      }
      $array = array("orderNo"=>$orderID,
                  "customerCode"=>$customerCode,
                  "description"=>"Testing",
                  "dateOrdered"=>$orderDate,
                  "salesrep"=>"",
                  "warehouseId"=>"",
                  "invoiceRule"=>"After Delivery",
                  "paymentType"=> $pay_type,
                  "orderLineList"=> $productsArray,
                );
      array_push($Order_Array,$array);
}

if (!empty($Order_Array)) {
  foreach ($Order_Array as $finalOrder) {
    $finalJson = json_encode($finalOrder);
    //Send Order into POS
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/salesorder/createorder?access_token=".$token_id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 100,
      CURLOPT_TIMEOUT => 300,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $finalJson,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      pos_error_log("cURL Error #:" . $err);
      pos_error_log("============================================================");
      pos_error_log("SEND ORDERS FAILD");
      pos_error_log("============================================================");
    } else {
      pos_error_log($response);
      pos_error_log("============================================================");
      pos_error_log("SEND ORDERS");
      pos_error_log("============================================================");
      // echo $response;
    }
  }
}
