<?php
$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require_once $root.'/wp-load.php' ;

//GET TOKEN TO ACCESS POS
include_once( dirname( __FILE__ ) . '/get_token.php' );
$token_id = token_genrate();

global $wpdb;
$table_name = $wpdb->prefix . 'POS_cate_map';

echo "<br>".$offsetPos = $_POST['offsetPos'];
echo "<br>".$limitPos = $_POST['limitPos'];
echo "<br>".$productCount = $_POST['productCount'];

if($offsetPos == 0){
    pos_error_log("============================================================");
    pos_error_log("DATA SYNC START");
    pos_error_log("============================================================");

    $params = array();
    parse_str($_POST['data'], $params);

    for ($i=0; $i < count($params['id']); $i++) {

        $pos_id = $params['id'][$i];
        $pos_name = $params['name'][$i];
        $pos_parent = $params['parent'][$i];
        $params['map_data'][$i];
        $exploded_value = explode('|', $params['map_data'][$i]);
        $woo_id = $exploded_value[0];
        $woo_name = $exploded_value[1];
        $woo_parent = $exploded_value[2];

        $check_exists_cate = $wpdb->get_results("SELECT * FROM $table_name WHERE POS_cate_Id=".$pos_id);

        if (count($check_exists_cate)==0) {
            $wpdb->insert(
                $table_name,array(
                'Woo_cate_name'     => $woo_name,
                'Woo_cate_Id'       => $woo_id,
                'Woo_cate_parent_Id'=> $woo_parent,
                'POS_cate_name'     => $pos_name,
                'POS_cate_Id'       => $pos_id,
                'POS_cate_parent_Id'=> $pos_parent
                )
            );
        }else{
            $where = array('POS_cate_Id' => $pos_id);
            $wpdb->update(
                $table_name,array(
                'Woo_cate_name'     => $woo_name,
                'Woo_cate_Id'       => $woo_id,
                'Woo_cate_parent_Id'=> $woo_parent,
                'POS_cate_name'     => $pos_name,
                'POS_cate_Id'       => $pos_id,
                'POS_cate_parent_Id'=> $pos_parent
                ),$where
            );
        }
    }

    $get_map_data = $wpdb->get_results("SELECT * FROM $table_name");

    $ins_cate = array();
    foreach ($get_map_data as $row){ 

        if ($row->Woo_cate_name==NULL) {
            array_push($ins_cate, (object)array('id' => $row->POS_cate_Id, 'name' => $row->POS_cate_name, 'parent' => $row->POS_cate_parent_Id));
        }
    }

    function buildTree($ins_cate) {
        $item->parent;
        $childs = array();
        foreach($ins_cate as $item){
            $childs[$item->parent][] = $item;
            foreach($ins_cate as $item){
                if (isset($childs[$item->id])){
                    $item->childs = $childs[$item->id];

                }
            }
        }return $childs[0];
    }

    $tree = buildTree($ins_cate);

    foreach ($tree as $result) {
        $args = array(
            'taxonomy'   => "product_cat",
            'name'       => $result->name,
            'hide_empty' => false,
        );
        $product_categories = get_terms($args);
        if (count($product_categories)==1) {

        }else{
            $post_id = wp_insert_term( $result->name, 'product_cat', array(
                    'parent' => 0, // optional
            ) );
        }
        wp_set_object_terms($post_id, $cats, 'product_cat');
        if (isset($result->childs)) {
            $get_last_id = $post_id['term_id'];
            foreach ($result->childs as $value) {
                $args = array(
                    'taxonomy'   => "product_cat",
                    'name'       => $value->name,
                    'hide_empty' => false,
                );
                $product_categories = get_terms($args);
                if ($get_last_id==NULL) {
                    $get_last_id = $product_categories[0]->parent;
                }

                if (count($product_categories)==1) {

                }else{
                    $post_child_id = wp_insert_term( $value->name, 'product_cat', array(
                            'parent' => $get_last_id, // optional
                    ) );
                }
                wp_set_object_terms($post_child_id, $subcats, 'product_cat');
            }
        }  
    }

    include_once( dirname( __FILE__ ) . '/get_products.php' );
    // echo "products ";

    include_once( dirname( __FILE__ ) . '/create_user.php' );
    // echo "products ";

    include_once( dirname( __FILE__ ) . '/get_customers.php' );
    // echo "get customers";

    // include_once( dirname( __FILE__ ) . '/create_order.php' );
    // echo "careate order";
}else{

    include_once( dirname( __FILE__ ) . '/get_products.php' );
    // echo "products ";
}
    if ($offsetPos+$limitPos >= $productCount) {
        pos_error_log("============================================================");
        pos_error_log("DATA SYNC END");
        pos_error_log("============================================================");
    }
    