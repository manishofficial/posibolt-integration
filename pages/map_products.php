<?php

	$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
	require_once( $root.'/wp-load.php' );

	global $wpdb;
	$map_pro_table = $wpdb->prefix . 'POS_product_map';

	include_once( dirname( __FILE__ ) . '/get_token.php' );
	$token_id = token_genrate();

	$api_url = str_replace(' ', '%20',get_option('wc_settings_pos_tab_url')); // get default url
	$current_time = strtotime(date('Y-m-d H:i:s'));

	function get_pos_fields( $token_id ,$api_url,$current_time ){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/productmaster/productlist?updatedSince=".$current_time."&webstoreFeaturedOnly=true&access_token=".$token_id."&limit=1",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Accept: */*",
		    "Cache-Control: no-cache",
		    "Connection: keep-alive",
		    "Host: autostyle.posibolt.com",
		    "Postman-Token: 977dcdcf-72b5-4ce8-bce6-00c601be97d1,2f489021-f133-4a2c-9eec-94f11c746c74",
		    "User-Agent: PostmanRuntime/7.15.0",
		    "accept-encoding: gzip, deflate",
		    "cache-control: no-cache",
		    "cookie: JSESSIONID=3D6EBB6307CBE44C59EDA2F2050B9C10"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			$getProduct = json_decode($response);
			$getProduct = array_shift($getProduct);
			$posProKeys = array_keys((array)$getProduct);
		}
		// $posProKeys = array('test 1','test 2','test 3','test 4','test 5','test 6','test 7','test 8','test 9','test 10','test 11','test 12');
		return $posProKeys;
	}

	function get_woo_producst_fields(){
		$WooProKeys = array(
				'post_title',
				'post_content',
				'post_excerpt',
				'_regular_price',
				'_sale_price',
				'_sku',
				'_stock',
		);

		return $WooProKeys;
	}

?>
	<div class="pro_title" id="pro_title">
        <center><h2><u>MAP PRODUCTS FIELDS</u></h2></center>
    </div>
    <div class="map-products">
    	<div class="woo-fields">
    		<form id="mapProducts" method="post">
    			<table align="center" cellpadding="2" cellspacing="5" style="border: 1px solid">
	    			<tr>
	    				<th>Woocommerce Product Fields</th>
	    				<th>POS Product Fields</th>
	    			</tr>
					<?php $wooKeys = get_woo_producst_fields(); ?>
					<?php $posKeys = get_pos_fields( $token_id ,$api_url,$current_time ); ?>
					<?php foreach ($wooKeys as $wookey) { ?>
						<tr>
							<td style="border: 1px solid">
								<?php echo $wookey ?>
							</td>
	    					<td align="center">
	    						<?php $get_pro_map_keys = $wpdb->get_results("SELECT * FROM $map_pro_table WHERE woo_pro_field='".$wookey."'"); ?>
	    						<select name="<?php echo $wookey ?>" required>
	    							<option value="">Please Select</option>
	    							<option value="none" <?php if ($get_pro_map_keys[0]->POS_pro_field == 'none') { echo "selected";} ?>>None</option>
	    							<?php foreach ($posKeys as $poskey) { ?>
	    							<option value="<?php echo $poskey ?>" <?php if ($get_pro_map_keys[0]->POS_pro_field == $poskey) { echo "selected";} ?>><?php echo $poskey ?></option>
	    							<?php }	?>
	    						</select>
	    					</td>
						</tr>
					<?php }

					$check_box_val = $wpdb->get_results("SELECT POS_pro_field FROM $map_pro_table WHERE woo_pro_field='groupcheckval' OR woo_pro_field='groupmap'");
					?>
					<tr>
						<td>
							<input type="checkbox" name="groupcheck" id="groupcheck" <?php if ( $check_box_val[0]->POS_pro_field=='true' ) {echo 'checked';} ?>>Enable Group 1 Prefix for Brand
							<input type="hidden" name="groupcheckval" id="groupcheckval" value="<?php if ($check_box_val[0]->POS_pro_field=='true' ) {echo 'true';}else{echo 'false';} ?>">
						</td>
					</tr>
					<tr>
						<td>
							<select name="groupmap" id="setgroup" <?php if ($check_box_val[0]->POS_pro_field=='false' || $check_box_val[0]->POS_pro_field == '') {echo 'disabled';} ?>>
								<option value="">Please Select</option>
								<option value="post_title" <?php if ($check_box_val[1]->POS_pro_field == 'post_title'){echo "selected";} ?>>Woocommerce Product Name</option>
								<option value="post_excerpt"<?php if ($check_box_val[1]->POS_pro_field == 'post_excerpt'){echo "selected";} ?>>woocommerce Short Discription</option>
								<option value="post_content"<?php if ($check_box_val[1]->POS_pro_field == 'post_content'){echo "selected";} ?>>woocommerce Long Discription</option>
								<option value="_sku"<?php if ($check_box_val[1]->POS_pro_field == '_sku'){echo "selected";} ?>>SKU</option>
							</select>
						</td>
						<td style="max-width: 320px;">If this option is ticked, the Group 1 value from the Posibolt API must be prepended to the relevant field with a space after word in the dropdown below.</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" name="continue_sync" value="Save & Continue Sync" style="border: none;width:100%;color: white;cursor: pointer;background: #467ca6;">
						</td>
					</tr>
    			</table>
    		</form>
    	</div>
    </div>
    <div id="result"></div>
<script type="text/javascript">
	jQuery('#groupcheck').on('change', function() {
	    // var checked = this.checked
	    if (this.checked == true) {
	    	jQuery('#setgroup').prop("disabled", false);
	    	jQuery('#groupcheckval').val("true");
	    }else{
	    	jQuery('#setgroup').prop("disabled", true);
	    	jQuery('#groupcheckval').val("false");
	    }
	});
	
	jQuery('form').submit(function(e) {
		e.preventDefault();
	    // Get all the forms elements and their values in one step
	    var data = jQuery(this).serialize();
	    jQuery('.map-products').hide();
	    jQuery('#pro_title').hide();
	    jQuery('#Load').show();
	    jQuery.ajax({
            type: "POST",
            url: "<?php echo plugin_dir_url( __FILE__ ).'map_category.php'; ?>",
            data: {data:data},
            success: function(data) {
            	jQuery('#Load').hide();
            	jQuery("#result").html(data);
            },
            error: function(data){
            	alert('something wrong : ' + data);
            }
        });
	});
</script>

