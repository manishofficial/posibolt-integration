<?php
//GET CUSTOMERS FROM POS
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/customermaster/customerslist?access_token=".$token_id,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 100,
  CURLOPT_TIMEOUT => 300,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  // echo "cURL Error #:" . $err;
  pos_error_log("cURL Error #:" . $err);
  pos_error_log("============================================================");
  pos_error_log("GET CUSTOMERS");
  pos_error_log("============================================================");
} else {
  $get_all_customers = json_decode($response);

  //Insertion Querry

    foreach ($get_all_customers as $customer) {

      $getwpuser = get_users(array('meta_key'=>'POS_customer_code','meta_value'=>$customer->customerCode));

      if (empty($getwpuser)) {
        $user_id = wp_create_user( $customer->name, wp_generate_password(), $customer->email );
        $user_id_role = new WP_User($user_id);
        $user_id_role->set_role('customer');
      }else{
        $user_id = $getwpuser[0]->ID;
        wp_update_user(
          array(
            'ID'       => $user_id,
            'user_email' => $customer->email
          )
        );
      }

      update_user_meta( $user_id, "billing_first_name", '' );
      update_user_meta( $user_id, "billing_last_name", '' );
      update_user_meta( $user_id, "billing_company", '' );
      update_user_meta( $user_id, "billing_address_1", $customer->address1 );
      update_user_meta( $user_id, "billing_address_2", $customer->address2 );
      update_user_meta( $user_id, "billing_city", $customer->city );
      update_user_meta( $user_id, "billing_postcode", $customer->postalCode );
      update_user_meta( $user_id, "billing_country", $customer->country );
      update_user_meta( $user_id, "billing_state", '' );
      update_user_meta( $user_id, "billing_email", $customer->email );
      update_user_meta( $user_id, "billing_phone", $customer->phone );

      // Shipping Details
      update_user_meta( $user_id, "shipping_first_name", '' );
      update_user_meta( $user_id, "shipping_last_name", '' );
      update_user_meta( $user_id, "shipping_company", '' );
      update_user_meta( $user_id, "shipping_address_1", $customer->address1 );
      update_user_meta( $user_id, "shipping_address_2", $customer->address2 );
      update_user_meta( $user_id, "shipping_city", $customer->city );
      update_user_meta( $user_id, "shipping_postcode", '' );
      update_user_meta( $user_id, "shipping_country", $customer->country );
      update_user_meta( $user_id, "shipping_state", '' );
      update_user_meta( $user_id, "POS_customer_code", $customer->customerId );
    }
}