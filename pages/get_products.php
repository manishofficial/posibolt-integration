<?php
$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require_once($root.'/wp-admin/includes/image.php');

// get current date and time
$current_time = strtotime(date('Y-m-d H:i:s'));

$api_url = str_replace(' ', '%20',get_option('wc_settings_pos_tab_url'));

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/productmaster/productlist?updatedSince=".$current_time."&limit=".$limitPos."&offset=".$offsetPos."&webstoreFeaturedOnly=true&access_token=".$token_id,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  // echo "cURL Error #:" . $err;
  pos_error_log("cURL Error #:" . $err);
  pos_error_log("============================================================");
  pos_error_log("GET PRODUCT");
  pos_error_log("============================================================");
} else {
  $posProducts = json_decode($response);
  // echo "<pre>"; print_r($posProducts); die();
}

foreach ($posProducts as $posProduct) {
  $posProductId = $posProduct->productId; 
  $baseProductId = $posProduct->baseProductId;
  if ($baseProductId !== 0 ) {
    varriationProductUpdate($posProduct,$posProductId,$baseProductId,$api_url,$token_id,$root);
  }else{
    simpleProductUpdate($posProduct,$posProductId,$api_url,$token_id,$root);
  }
}

function varriationProductUpdate($item,$product_id,$baseProductId,$api_url,$token_id,$root){
    $searchType = 'variation';
    $productExistIds = checkExistOrDeleteImage($product_id,$searchType); //GET EXISTING ID OR DELETE IMAGE

    $args = array(
      "post_type" => "product",
      "meta_query" => array(
          array(
            "key"     => "POS_product_id",
            "value"   => $baseProductId,
            "compare" => "EXISTS",
          ),
      ),
    );

    $query = new WP_Query( $args );
    $parent_id = $query->posts[0]->ID;
    wp_set_object_terms( $parent_id, 'variable', 'product_type' );
    $product = wc_get_product($parent_id);
      //SET PRODUCT ATTRIBUTES
      $attributes = array();
      $ai = 0;
      $countArr = count($item->tags);
      $tags = $item->tags;
      while ($ai < $countArr) {
        // $proAttr = $product->get_attribute( $tags[$ai]->name );
        $proAttr = get_post_meta( $product->id , '_product_attributes' );
        $proAttr = array_shift($proAttr);
        $proAttr = $proAttr[$tags[$ai]->name]['value'];
        if ($proAttr==NULL) {
          $Final = implode('|',array_unique(explode('|', $tags[$ai]->value)));
        }else{
          $Final = implode('|',array_unique(explode('|', $proAttr."|".$tags[$ai]->value)));
        }

        $Att[$tags[$ai]->name] = array( "name"  =>$tags[$ai]->name,
                          "value"       =>$Final,
                          "position"    =>$ai,
                          "is_visible"  =>1,
                          "is_variation"=>1,
                          "is_taxonomy" => 0
                    );
        $attributes = $Att;
        // array_push($attributes, $Att);
        $ai++;
      }
    
      // echo "<pre>"; print_r($attributes); echo "</pre>"; die;
      update_post_meta( $parent_id, '_product_attributes', $attributes );
      update_post_meta( $parent_id, '_manage_stock', 'no' );

      $proDesc = $item->description;
      if($item->detailedDescription != NULL){
        $proDesc = $item->detailedDescription;
      }

      if ($item->upc != NULL) {
        $sku = $item->upc;
      }elseif ($item->sku != NULL){
        $sku = $item->sku;
      }else{
        $sku = $parent_id;
      }

      $var_id = $productExistIds[1];

      if ($var_id==NULL) {
    // product insertion
          $variation = array(
              'post_title'   => get_map_data('post_title',$item). ' (variation)',
              'post_name'    => 'product-'.$item->productId . '- (variation)',
              'post_content' => get_map_data('post_content',$item),
              'post_status'  => 'publish',
              'post_parent'  => $parent_id,
              'post_type'    => 'product_variation',
              'post_excerpt' => get_map_data('post_excerpt',$item),
              'guid'         => home_url().'/?product_variation='.$item->name
          );

          $variation_id = wp_insert_post( $variation );
      }else{
          $variation_id = $var_id;
          $variation_update = array(
            'post_title'   => get_map_data('post_title',$item) . ' (variation)',
            'post_name'    => 'product-'.$item->productId . '- (variation)',
            'post_content' => get_map_data('post_content',$item),
            'post_excerpt' => get_map_data('post_excerpt',$item),
          );
          wp_update_post( $variation_update );
      }

      //CHECK PROHUCT QUANTITY FOR IN STOCK OR OUT OF STOCK
      $stock_staus = "outofstock";
      if ($item->stockQty>0) {
        $stock_staus = "instock";
      }
      
      $i = 0;
      $countArr = count($item->tags);
      while ($i < $countArr) {
          $tagName = $item->tags[$i]->name;
          $tagValue = $item->tags[$i]->value;
         update_post_meta( $variation_id, 'attribute_'.strtolower($tagName), $tagValue );
        $i++;
      }

      update_post_meta( $variation_id, '_variation_description', get_map_data('post_content',$item) );
      update_post_meta( $variation_id, '_price', get_map_data('_regular_price',$item) );
      update_post_meta( $variation_id, '_regular_price', get_map_data('_regular_price',$item) );
      update_post_meta( $variation_id, '_sku', get_map_data('_sku',$item) );
      update_post_meta( $variation_id, '_stock_qty', get_map_data('_stock_qty',$item) );
      update_post_meta( $variation_id, '_virtual', 'no' );
      update_post_meta( $variation_id, '_downloadable', 'no' );
      update_post_meta( $variation_id, '_manage_stock', 'yes' );
      update_post_meta( $variation_id, '_stock_status', $stock_staus );
      update_post_meta( $variation_id, 'POS_custom_field', $product_id );

      update_post_meta($variation_id, '_visibility', 'visible' );
      update_post_meta($variation_id, 'total_sales', '0');
      update_post_meta($variation_id, '_purchase_note', "" );
      update_post_meta($variation_id, '_featured', "no" );
      update_post_meta($variation_id, '_weight', "" );
      update_post_meta($variation_id, '_length', "" );
      update_post_meta($variation_id, '_width', "" );
      update_post_meta($variation_id, '_height', "" );
      update_post_meta($variation_id, '_sale_price_dates_from', "" );
      update_post_meta($variation_id, '_sale_price_dates_to', "" );
      update_post_meta($variation_id, '_sold_individually', "" );
      update_post_meta($variation_id, '_backorders', "no" );
      update_post_meta($variation_id, '_stock', get_map_data('_stock',$item) );

      WC_Product_Variable::sync( $parent_id );
}

function simpleProductUpdate($item,$product_id,$api_url,$token_id,$root){
  $searchType = 'simple';
  $productExistIds = checkExistOrDeleteImage($product_id,$searchType); //GET EXISTING ID OR DELETE IMAGE
  //SET DISCRIPTION
  $proDesc = $item->description;
  if($item->detailedDescription != NULL){
    $proDesc = $item->detailedDescription;
  }

  if ($productExistIds[0]=='') {
  // PRODUCT INSERTION
    $post_id = wp_insert_post( array(
        'post_title' => get_map_data('post_title',$item), //$item->name,
        'post_content' => get_map_data('post_content',$item),
        'post_status' => 'publish',
        'post_excerpt' => get_map_data('post_excerpt',$item),
        'post_type' => "product",
    ) );
  }else{
    //PRODUCT UPDATE
    $post_id = $productExistIds[0];

    $my_post = array(
        'ID'           => $post_id,
        'post_title'   => get_map_data('post_title',$item),
        'post_content' => get_map_data('post_content',$item),
        'post_excerpt' => get_map_data('post_excerpt',$item)
    );
    wp_update_post( $my_post );
  }
  //SET SKU
  $sku = $post_id;
  if ($item->sku != NULL){
    $sku = $item->sku;
  }
  //CHECK PROHUCT QUANTITY FOR IN STOCK OR OUT OF STOCK
  $stock_staus = "outofstock";
  if ($item->stockQty>0) {
    $stock_staus = "instock";
  }
  //update data fields
  wp_set_object_terms( $post_id, 'simple', 'product_type' );

  update_post_meta( $post_id, '_visibility', 'visible' );
  update_post_meta( $post_id, '_stock_status', $stock_staus );
  update_post_meta( $post_id, 'total_sales', '0' );
  update_post_meta( $post_id, '_downloadable', 'no' );
  update_post_meta( $post_id, '_virtual', 'no' );
  update_post_meta( $post_id, '_price', get_map_data('_regular_price',$item) );
  update_post_meta( $post_id, '_regular_price', get_map_data('_regular_price',$item) );
  update_post_meta( $post_id, '_sale_price', get_map_data('_sale_price',$item) );
  update_post_meta( $post_id, '_purchase_note', '' );
  update_post_meta( $post_id, '_featured', 'no' );
  update_post_meta( $post_id, '_weight', '' );
  update_post_meta( $post_id, '_length', '' );
  update_post_meta( $post_id, '_width', '' );
  update_post_meta( $post_id, '_height', '' );
  update_post_meta( $post_id, '_sku', get_map_data('_sku',$item) );
  update_post_meta( $post_id, '_product_attributes', '' );
  update_post_meta( $post_id, '_sale_price_dates_from', '' );
  update_post_meta( $post_id, '_sale_price_dates_to', '' );
  update_post_meta( $post_id, '_sold_individually', '' );
  update_post_meta( $post_id, '_manage_stock', 'yes' );
  update_post_meta( $post_id, '_backorders', 'no' );
  update_post_meta( $post_id, '_stock', get_map_data('_stock',$item) );
  update_post_meta( $post_id, 'POS_product_id', $item->productId );

  updateCategory($item->productCategory,$post_id);
}

function checkExistOrDeleteImage($product_id,$searchType){
  if ($searchType == 'variation') {
    $args = array(
      "post_type" => "product_variation",
      "meta_query" => array(
          array(
              "key"     => "POS_custom_field",
              "value" => $product_id,
              "compare" => "EXISTS",
          ),
      ),
    );
  }
  if ($searchType == 'simple') {
    $args = array(
      "post_type" => "product",
      "meta_query" => array(
          array(
              "key"     => "POS_product_id",
              "value" => $product_id,
              "compare" => "EXISTS",
          ),
      ),
    );
  }
  $query = new WP_Query( $args );
  $productExistId = '';
  $var_id = '';
  if ( isset($query->post->ID) ) {
    $productExistId = $query->post->ID;
    $img_data = wp_get_attachment_image_src( get_post_thumbnail_id( $productExistId ), 'single-post-thumbnail' );
    $img_file = $img_data[0];
    $attachment_id = attachment_url_to_postid($img_file);
    // wp_delete_attachment( $attachment_id );
    $var_id = $query->posts[0]->ID;
  }
  $returnarray = array($productExistId,$var_id);
  return $returnarray;
}
function updateCategory($productCategory, $post_id){
  global $wpdb;
    $table_name = $wpdb->prefix . 'POS_cate_map';

    $get_map_cate = $wpdb->get_results("SELECT * FROM $table_name WHERE POS_cate_name='".$productCategory."'");

    if ($get_map_cate[0]->Woo_cate_name==NULL) {
        wp_set_object_terms($post_id, $productCategory, 'product_cat');
    }else{
      wp_set_object_terms($post_id, $get_map_cate[0]->Woo_cate_name, 'product_cat');
    }
}

//GET MAPPED DATA
function get_map_data($key ,$item){
  global $wpdb;
  $map_pro_table = $wpdb->prefix . 'POS_product_map';
  $get_map_fields = $wpdb->get_results("SELECT POS_pro_field FROM $map_pro_table WHERE woo_pro_field='".$key."'");
  if ($get_map_fields[0]->POS_pro_field == 'priceArray') {
    $final_value = $item->priceArray[0];
  }else{
    $data_value = $get_map_fields[0]->POS_pro_field;
    $final_value = $item->$data_value;
  }

  if ($get_map_fields[0]->POS_pro_field == 'productId') {
    if ($item->productId==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'description') {
    if ($item->description==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'name') {
    if ($item->name==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'productCategoryId') {
    if ($item->productCategoryId==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'model') {
    if ($item->model==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'isActive') {
    if ($item->isActive==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'stockQty') {
    if ($item->stockQty==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'salesPrice') {
    if ($item->salesPrice==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'costPrice') {
    if ($item->costPrice==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'productCategory') {
    if ($item->productCategory==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'tags') {
    if ($item->tags==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'created') {
    if ($item->created==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'uomId') {
    if ($item->uomId==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'uom') {
    if ($item->uom==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'unitsPerPack') {
    if ($item->unitsPerPack==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'taxCategoryID') {
    if ($item->taxCategoryID==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'baseProductId') {
    if ($item->baseProductId==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'sku') {
    if ($item->sku==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'upc') {
    if ($item->upc==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'group1') {
    if ($item->group1==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'group2') {
    if ($item->group2==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'webstoreFeatured') {
    if ($item->webstoreFeatured==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'detailedDescription') {
    if ($item->detailedDescription==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'upcType') {
    if ($item->upcType==NULL) {
      $final_value = "";
    }
  }
  if ($get_map_fields[0]->POS_pro_field == 'tagSet') {
    if ($item->tagSet==NULL) {
      $final_value = "";
    }
  }

  $get_group_map_field = $wpdb->get_results("SELECT POS_pro_field FROM $map_pro_table WHERE woo_pro_field='groupcheckval'");

  if ($get_group_map_field[0]->POS_pro_field == 'true') {
    $get_group_map_field_val = $wpdb->get_results("SELECT POS_pro_field FROM $map_pro_table WHERE woo_pro_field='groupmap'");
    if (!empty($get_group_map_field_val)) {
      if ($get_group_map_field_val[0]->POS_pro_field == $key) {
        $map_val = $item->group1." ";
      }else{
        $map_val = "";
      }
    }
  }

  return $map_val.$final_value;
}
