<?php
function order_status_webhook( $orderID, $status_from, $status_to, $instance ){
	//GET POS TOKEN
	include_once( dirname(dirname( __FILE__ )) . '/get_token.php' );
	$token_id = token_genrate();

  $api_url = str_replace(' ', '%20',get_option('wc_settings_pos_tab_url')); //GET POS SYSTEM URL
  //DATA FOR SEND AND ENCODE INTO JASON
    $finalJson = json_encode(array("orderNo"=>$orderID,
                   "status"	=>$status_to,
                ));
    //SEND ORDER STATUS IN POS
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/salesorder/updatestatus?access_token=".$token_id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 100,
      CURLOPT_TIMEOUT => 300,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $finalJson,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      // echo "<br>cURL Error #:" . $err;
      // pos_error_log("cURL Error #:" . $err);
      // pos_error_log("============================================================");
      // pos_error_log("SEND ORDERS STATUS FAILD");
      // pos_error_log("============================================================");
    } else {
      // pos_error_log($response);
      // pos_error_log("============================================================");
      // pos_error_log("SEND ORDERS STATUS");
      // pos_error_log("============================================================");
      // echo $response;
    }
}