<?php
function send_order_pos( $order_id ){
	$file_pointer = dirname(plugin_dir_path( __FILE__ )).'/logs/php_error.log';
	// using unlink() function to delete a file 
	unlink($file_pointer);

	// get order object and order details
	include_once( dirname(dirname( __FILE__ )) . '/get_token.php' );
	$token_id = token_genrate();
	if ($token_id == '') {
		pos_error_log("TOKEN NOT CREATED");
	}
	$api_url = str_replace(' ', '%20',get_option('wc_settings_pos_tab_url')); // get default url
	$order = new WC_Order($order_id);

	//$usersId = $order->user_id;
	$userId = $order->get_customer_id();
	//CHECK USER HAVE POS ID IF USER NOT HAVE POS ID THEN SYNC USER TO POS
	$user_pos_id = get_user_meta( $userId, 'POS_customer_code', true);
	if ($order->get_status()=='processing') {
	    $user_pos_id = get_user_meta( $userId, 'POS_customer_code', true); //USER POS ID
	    $get_user_data=get_userdata($userId);
	    $get_user_role = array('not found');
	    if(!empty($get_user_data)){
	    	$get_user_role=$get_user_data->roles;
	    }

	    if ($get_user_role[0]=='customer') {
		    if ($user_pos_id == NULL) {
		        $user_data = array(
		            	"customerCode"  => $userId."-woo-".get_bloginfo( 'name' ),
		            	"name" 			=> $order->get_shipping_first_name(),
						"name2" 		=> $order->get_shipping_last_name(),
						"address1" 		=> $order->get_shipping_address_1(),
						"address2" 		=> $order->get_shipping_address_2(),
						"city" 			=> $order->get_shipping_city(),
						"mobile"		=> $order->get_billing_phone(),
						"country" 		=> WC()->countries->countries[ $order->get_shipping_country() ],
						"postalCode" 	=> $order->get_shipping_postcode(),
						"email" 		=> $order->get_billing_email(),
						"active" 		=> true,
						"action" 		=> "create"
		            );
		        $finalJson = json_encode($user_data);

		        $customerCode = $userId."-woo-".get_bloginfo( 'name' );

		      //CREATE WOOCOMMERCE CUSTOMERS TO POS
		        $curl = curl_init();

		        curl_setopt_array($curl, array(
		        CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/customermaster?access_token=".$token_id,
		        CURLOPT_RETURNTRANSFER => true,
		        CURLOPT_ENCODING => "",
		        CURLOPT_MAXREDIRS => 10,
		        CURLOPT_TIMEOUT => 30,
		        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		        CURLOPT_CUSTOMREQUEST => "POST",
		        CURLOPT_POSTFIELDS => $finalJson,
		        CURLOPT_HTTPHEADER => array(
		            "Accept: application/json",
		            "Authorization: Bearer ".$token_id,
		            "Content-Type: application/json",
		          ),
		        ));
		        $response = curl_exec($curl);
		        $err = curl_error($curl);
		        curl_close($curl);
		        if ($err) {
		              pos_error_log("cURL Error #:" . $err);
		              pos_error_log("============================================================");
		              pos_error_log("CUSTOMERS SEND TO POS FAILD");
		              pos_error_log("============================================================");
		        } else {
		              pos_error_log($response);
		              pos_error_log("============================================================");
		              pos_error_log("CUSTOMERS SEND STATUS");
		              pos_error_log("============================================================");

		              $custResponce = (array)json_decode($response);

		              // echo $response;
		              if ($custResponce['recordNo'] != null) {
			            update_user_meta( $userId, "POS_customer_code", $custResponce['recordNo'] );
			          }
			          $user_pos_id = $userId."-woo-".get_bloginfo( 'name' );
		        }
		    }else{
		    	$customerExists = getCustomerExists($user_pos_id,$token_id,$api_url);
		    	if($customerExists !== ''){
		    		$customerCode = $customerExists;
		    	}else{
		    		$user_data = array(
			            "customerCode"  => $userId."-woo-".get_bloginfo( 'name' ),
		            	"name" 			=> $order->get_shipping_first_name(),
						"name2" 		=> $order->get_shipping_last_name(),
						"address1" 		=> $order->get_shipping_address_1(),
						"address2" 		=> $order->get_shipping_address_2(),
						"city" 			=> $order->get_shipping_city(),
						"mobile"		=> $order->get_billing_phone(),
						"country" 		=> WC()->countries->countries[ $order->get_shipping_country() ],
						"postalCode" 	=> $order->get_shipping_postcode(),
						"email" 		=> $order->get_billing_email(),
						"active" 		=> true,
						"action" 		=> "create"
					);
			        $finalJson = json_encode($user_data);
		    		$customerCode = createCustomer($finalJson,$token_id,$api_url);
		    	}
		    	
		    }
	    }else{
	      	if ($user_pos_id == NULL) {
	        //CREATE GUEST AS WOOCOMMERE CUSTOMER AND SYNC TO POS
		      	if ($get_user_role[0]=='administrator') {
		      		$user_id = $userId;
		      		//update_user_meta( $user_id, "POS_customer_code", $user_id."-woo-".get_bloginfo( 'name' ) );
		      		$customerCode = $user_id."-woo-".get_bloginfo( 'name' );
		      	}else{
		      		$user = get_user_by( 'email', $order->get_billing_email() );
		      		if(isset($user->ID)){
		      			$user_id = $user->ID;
		      		}else{
		      			$userName = $order->get_billing_first_name()."".time();
		      			$user_id = wp_create_user( $userName, wp_generate_password(), $order->get_billing_email() );
		      		}
		      		$user_id_role = new WP_User($user_id);
			        $user_id_role->set_role('customer');
			        update_user_meta( $user_id, "billing_first_name", $order->get_billing_first_name() );
			        update_user_meta( $user_id, "billing_last_name", $order->get_billing_last_name() );
			        update_user_meta( $user_id, "billing_company", $order->get_billing_company() );
			        update_user_meta( $user_id, "billing_address_1", $order->get_billing_address_1() );
			        update_user_meta( $user_id, "billing_address_2", $order->get_billing_address_2() );
			        update_user_meta( $user_id, "billing_city", $order->get_billing_city() );
			        update_user_meta( $user_id, "billing_postcode", $order->get_billing_postcode() );
			        update_user_meta( $user_id, "billing_country", $order->get_billing_country() );
			        update_user_meta( $user_id, "billing_state", $order->get_billing_state() );
			        update_user_meta( $user_id, "billing_email", $order->get_billing_email() );
			        update_user_meta( $user_id, "billing_phone", $order->get_billing_phone() );
			        // Shipping Details
			        update_user_meta( $user_id, "shipping_first_name", $order->get_shipping_first_name() );
			        update_user_meta( $user_id, "shipping_last_name", $order->get_shipping_last_name() );
			        update_user_meta( $user_id, "shipping_company", $order->get_shipping_company() );
			        update_user_meta( $user_id, "shipping_address_1", $order->get_shipping_address_1() );
			        update_user_meta( $user_id, "shipping_address_2", $order->get_shipping_address_2() );
			        update_user_meta( $user_id, "shipping_city", $order->get_shipping_city() );
			        update_user_meta( $user_id, "shipping_postcode", $order->get_shipping_postcode() );
			        update_user_meta( $user_id, "shipping_country", $order->get_shipping_country() );
			        update_user_meta( $user_id, "shipping_state", $order->get_shipping_state() );
			        //update_user_meta( $user_id, "POS_customer_code", $user_id."-woo-".get_bloginfo( 'name' ) );
			        update_post_meta( $order->get_id() , '_customer_user', $user_id );

			        $customerCode = $user_id."-woo-".get_bloginfo( 'name' );
		      	}

		        $user_data = array(
		              	"customerCode"  => $customerCode,
		            	"name" 			=> $order->get_shipping_first_name(),
						"name2" 		=> $order->get_shipping_last_name(),
						"address1" 		=> $order->get_shipping_address_1(),
						"address2" 		=> $order->get_shipping_address_2(),
						"city" 			=> $order->get_shipping_city(),
						"mobile"		=> $order->get_billing_phone(),
						"country" 		=> WC()->countries->countries[ $order->get_shipping_country() ],
						"postalCode" 	=> $order->get_shipping_postcode(),
						"email" 		=> $order->get_billing_email(),
						"active" 		=> true,
						"action" 		=> "create"
		            );
		        $finalJson = json_encode($user_data);
		      //CREATE CUSTOMERS
		        $curl = curl_init();

		        curl_setopt_array($curl, array(
		        CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/customermaster?access_token=".$token_id,
		        CURLOPT_RETURNTRANSFER => true,
		        CURLOPT_ENCODING => "",
		        CURLOPT_MAXREDIRS => 10,
		        CURLOPT_TIMEOUT => 30,
		        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		        CURLOPT_CUSTOMREQUEST => "POST",
		        CURLOPT_POSTFIELDS => $finalJson,
		        CURLOPT_HTTPHEADER => array(
		            "Accept: application/json",
		            "Authorization: Bearer ".$token_id,
		            "Content-Type: application/json",
		          ),
		        ));
		        $response = curl_exec($curl);
		        $err = curl_error($curl);
		        curl_close($curl);
		        if ($err) {
		          	  pos_error_log("cURL Error #:" . $err);
		          	  pos_error_log("============================================================");
		              pos_error_log("CUSTOMERS SEND TO POS FAILD");
		              pos_error_log("============================================================");
		        } else {
		          	  pos_error_log($response);
		          	  pos_error_log("============================================================");
		              pos_error_log("CUSTOMERS SEND STATUS");
		              pos_error_log("============================================================");

		              $custResponce = (array)json_decode($response);

		              // echo $response;
		              if ($custResponce['recordNo'] != null) {
		              	update_user_meta( $user_id, "POS_customer_code", $custResponce['recordNo'] );
		              }
		              
		              // echo $response;
		        }
			}else{
				$customerExists = getCustomerExists($user_pos_id,$token_id,$api_url);
		    	if($customerExists !== ''){
		    		$customerCode = $customerExists;
		    	}else{
		    		$user_data = array(
			              	"customerCode"  => $user_id."-woo-".get_bloginfo( 'name' ),
			              	"name" 			=> $order->get_shipping_first_name(),
							"name2" 		=> $order->get_shipping_last_name(),
							"address1" 		=> $order->get_shipping_address_1(),
							"address2" 		=> $order->get_shipping_address_2(),
							"city" 			=> $order->get_shipping_city(),
							"mobile"		=> $order->get_billing_phone(),
							"country" 		=> WC()->countries->countries[ $order->get_shipping_country() ],
							"postalCode" 	=> $order->get_shipping_postcode(),
							"email" 		=> $order->get_billing_email(),
							"active" 		=> true,
							"action" 		=> "create"
			                    );
			        $finalJson = json_encode($user_data);
		    		$customerCode = createCustomer($finalJson,$token_id,$api_url);
		    	}
				//$customerCode = $user_pos_id;
			}
	    }
	}else{
		pos_error_log("ORDER IS NOT ON PROCESSING");
	    pos_error_log("============================================================");
      	pos_error_log("SEND ORDERS FAILD");
      	pos_error_log("============================================================");
      	goto skip;
	}

	//GET ALL ORDER REQUIRED DATA FOR POS
	$order_date = date( "d-m-Y", strtotime($order->get_date_created()));
	$paymenTitle = $order->get_payment_method_title(); //GET PAYMENT TITLE
	
	if ($paymenTitle=="Direct bank transfer") {
	  $pay_type = "EFT";
	}elseif ($paymenTitle=="Cash on delivery") {
	  $pay_type = "cash";
	}else{
	  $pay_type = "EFT";
	}

	$items = $order->get_items();
	$productsArray = array(); //define array to store products
	foreach ( $items as $item ) {
	    $item->get_name();
	    $product_var_id = $item->get_variation_id();
	    if( $product_var_id != NULL || $product_var_id != ''){
	    	$product_id = $product_var_id;
	    	$my_meta = get_post_meta( $product_id, 'POS_custom_field', true );
	    }else{
	    	$product_id = $item->get_product_id();
	    	$my_meta = get_post_meta( $product_id, 'POS_product_id', true );
	    }
	    $quantity = $item->get_quantity();
	    $total = $item->get_total();
	    array_push($productsArray,array("productId"=>$my_meta, "qty"=>$quantity, "price"=> $total)); 
	}
	$data = array("orderNo"=>$order_id,
	            "customerCode"=>$customerCode,
	            "description"=>"Testing",
	            "dateOrdered"=>$order_date,
	            "salesrep"=>"",
	            "warehouseId"=>"",
	            "invoiceRule"=>"After Delivery",
	            "paymentType"=> $pay_type,
	            "orderLineList"=> $productsArray,
	        );
	//SEND ORDER TO POS

    $finalJson = json_encode($data);
    //Send Order into POS
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/salesorder/createorder?access_token=".$token_id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 100,
      CURLOPT_TIMEOUT => 300,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $finalJson,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      pos_error_log("cURL Error #:" . $err);
      pos_error_log("============================================================");
      pos_error_log("SEND ORDERS FAILD");
      pos_error_log("============================================================");
    } else {
      pos_error_log($response);
      pos_error_log("============================================================");
      pos_error_log("SEND ORDERS");
      pos_error_log("============================================================");
      // echo $response;
    }
    skip : pos_error_log( "ORDER STATUS" );
}

function Status_change_order_sync($order_id , $checkout = null){
	//echo $order_id; die;
	$order = wc_get_order($order_id);
	if(isset($order) && !empty($order)){

		send_order_pos( $order_id );
	}
}

function showLogsAccordingOrderStatus($order_id) {
	$order = new WC_Order($order_id);
	$orderStatus = $order->get_status();
	pos_error_log("ORDER IS NOT ON PROCESSING");
    pos_error_log("============================================================");
  	pos_error_log("SEND ORDERS FAILD");
  	pos_error_log("===========================".$orderStatus."===========================");
}
//On Checkout update product quantity
function custom_checkout() {
	foreach ( WC()->cart->get_cart() as $cart_item ) {
       $product = $cart_item['data'];
       if(!empty($product)){
           $product_id = $product->get_id();
           $product_Type = $product->get_type();
           if ($product_Type == 'simple') {
           	 $product_attr = get_post_meta( $product_id, 'POS_product_id' );
           }elseif ($product_Type == 'variation') {
           	 $product_attr = get_post_meta( $product_id, 'POS_custom_field' );
           }
           
           if (!empty($product_attr)) {
           		$productPosId = $product_attr[0];

           		// get order object and order details
				include_once( dirname(dirname( __FILE__ )) . '/get_token.php' );
				$token_id = token_genrate();
				$api_url = str_replace(' ', '%20',get_option('wc_settings_pos_tab_url')); // get default url

           		//GET PRODUCT
		        $curl = curl_init();

		        curl_setopt_array($curl, array(
		        CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/productmaster/".$productPosId."?access_token=".$token_id,
		        CURLOPT_RETURNTRANSFER => true,
		        CURLOPT_ENCODING => "",
		        CURLOPT_MAXREDIRS => 10,
		        CURLOPT_TIMEOUT => 30,
		        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		        CURLOPT_CUSTOMREQUEST => "GET",
		        CURLOPT_HTTPHEADER => array(
		            "Accept: application/json",
		            "Authorization: Bearer ".$token_id,
		            "Content-Type: application/json",
		          ),
		        ));
		        $response = curl_exec($curl);
		        $err = curl_error($curl);
		        curl_close($curl);
		        if ($err) {
		          	  echo "cURL Error #:" . $err;
		        } else {
		          	  // pos_error_log($response);
		        	if ($response !== '' || $response !== NULL) {
		              	$productData = json_decode($response);
		              	$proStock = $productData->stockQty; 
		              	if ($proStock != '' || $proStock == '0') {
			              	update_post_meta( $product_id, '_stock', $proStock );
			              	if ($proStock<=0) {
	        					update_post_meta( $product_id, '_stock_status', 'outofstock' );
					        }
		              	}
		              	
		            }
		        }
           }
       }
    }
}

function getCustomerExists($posId, $token_id,$api_url){
	$custExistsResponce['customerCode'] = '';
	$curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/customermaster/".$posId."?access_token=".$token_id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Authorization: Bearer ".$token_id,
        "Content-Type: application/json",
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
          pos_error_log("cURL Error #:" . $err);
          pos_error_log("============================================================");
          pos_error_log("CUSTOMERS EXISTS TO POS FAILD");
          pos_error_log("============================================================");
    } else {

          $custExistsResponce = (array)json_decode($response);

    }
    return $custExistsResponce['customerCode'];
}

function createCustomer($userData,$token_id,$api_url){
	$user_pos_id = '';
	//CREATE WOOCOMMERCE CUSTOMERS TO POS
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => $api_url."/AdempiereService/PosiboltRest/customermaster?access_token=".$token_id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $userData,
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Authorization: Bearer ".$token_id,
        "Content-Type: application/json",
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
          pos_error_log("cURL Error #:" . $err);
          pos_error_log("============================================================");
          pos_error_log("CUSTOMERS SEND TO POS FAILD");
          pos_error_log("============================================================");
    } else {
          pos_error_log($response);
          pos_error_log("============================================================");
          pos_error_log("CUSTOMERS SEND STATUS");
          pos_error_log("============================================================");

          $custResponce = (array)json_decode($response);

          // echo $response;
          if ($custResponce['recordNo'] != null ) {
          	update_user_meta( $userId, "POS_customer_code", $custResponce['recordNo'] );
          }
          $user_pos_id = $userId."-woo-".get_bloginfo( 'name' );
    }
    return $user_pos_id;
}