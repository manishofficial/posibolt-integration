<?php

function my_plugin_add_settings() {
    /**
     * Settings class
     */
    class WC_Settings_pos extends WC_Settings_Page {
        /**
         * Setup settings class
         */
        public function __construct() {

            $this->id    = 'pos';
            $this->label = __( 'POSibolt Integration', 'my-textdomain' );

            add_filter( 'woocommerce_settings_tabs_array',array( $this, 'add_settings_page' ), 20 );

            add_action( 'woocommerce_settings_' . $this->id,array( $this, 'output' ) );

            add_action( 'woocommerce_settings_save_' . $this->id,array( $this, 'save' ) );

            add_action( 'woocommerce_sections_' . $this->id,array( $this, 'output_sections' ) );
       }
        /**
         * Get sections
         */
        public function get_sections() {

            $sections = array(
                'first'  => __( 'Posibolt Settings', 'my-textdomain' ),
                'second' => __( 'Sync Schedule', 'my-textdomain' ),
                'third' => __( 'Logs Status', 'my-textdomain' ),
            );
            return apply_filters( 'woocommerce_get_sections_' . $this->id, $sections );
        }
        /**
         * Get settings array
         */
        public function get_settings( $current_section = '' ) {
            /*
            * Filter Plugin Section 0 Settings
            */
            if ('' == $current_section) {
                //This will show error for details are wrong!
                include_once( dirname( __FILE__ ) . '/get_token.php' );
                $token_id = token_genrate();
                if (!get_option('wc_settings_pos_tab_url')==NULL) {
                    if ($token_id==NULL) {
                        echo "<div class='notice notice-error is-dismissible'>
                                <p>POS Details are wrong please check it again.</p>
                              </div>";
                    }else{
                        echo "<div class='notice notice-success is-dismissible'>
                                <p>You are connected with POS.</p>
                              </div>";
                    }
                }
                $file_pointer = dirname(plugin_dir_path( __FILE__ )).'/logs/php_error.log';
                global $wpdb;
                $table_name = $wpdb->prefix . 'POS_woo_api';
                $get_api_token = $wpdb->get_results("SELECT * FROM $table_name");
                if (!empty($get_api_token)) {
                    $statusApi = $get_api_token[0]->link;
                }else{
                    $statusApi = '';
                }
?>
                <h2>Woocommerce API</h2>
                <p>Click the button below to receive an API to update the order status</p>
                <button type="button" id="sync_dis" class="btn-success btn geturl" data-loading-text="Loading...">Click to get url</button><br><br>
                <div id="url_result">
                    <textarea rows="4" cols="70" readonly><?php echo $statusApi; ?>
                    </textarea>
                </div>
                <script type="text/javascript">
                    jQuery(function() {
                        jQuery(".geturl").click(function()
                        {
                            jQuery.post("<?php echo plugin_dir_url( __FILE__ ).'api/gen_api.php' ?>",function(data)
                            {
                                location.reload();
                            });
                        });
                    });
                </script>

                <style type="text/css">
                    .btn-success {
                        color: #fff;
                        background-color: #3a7aab;
                        border-color: #4cae4c;
                    }
                    .btn {
                        display: inline-block;
                        padding: 6px 12px;
                        margin-bottom: 0;
                        font-size: 14px;
                        font-weight: 400;
                        line-height: 1.42857143;
                        text-align: center;
                        white-space: nowrap;
                        vertical-align: middle;
                        -ms-touch-action: manipulation;
                        touch-action: manipulation;
                        cursor: pointer;
                        -webkit-user-select: none;
                        -moz-user-select: none;
                        -ms-user-select: none;
                        user-select: none;
                        background-image: none;
                        border: 1px solid transparent;
                        border-radius: 4px;
                    }
                    .wp-core-ui .button-primary{display: none;}
                </style>
<?php
            }
            $settings = array();
            if ('first' == $current_section) {
                /**
                * Filter Plugin Section 1 Settings
                */
                //This will show error for details are wrong!
                include_once( dirname( __FILE__ ) . '/get_token.php' );
                $token_id = token_genrate();
                if (!get_option('wc_settings_pos_tab_url')==NULL) {
                    if ($token_id==NULL) {
                        echo "<div class='notice notice-error is-dismissible'>
                                <p>POS Details are wrong please check it again.</p>
                              </div>";
                    }else{
                        echo "<div class='notice notice-success is-dismissible'>
                                <p>You are connected with POS.</p>
                              </div>";
                    }
                }
                $settings = array(
                    'section_title' => array(
                        'name'     => __( 'WooCommerce Posibolt Integration', 'woocommerce-settings-tab-demo' ),
                        'type'     => 'title',
                        'desc'     => '',
                        'id'       => 'wc_settings_pos_tab_section_title',
                        'placeholder'=>''
                    ),
                    'url' => array(
                        'name' => __( 'Posibolt domain url', 'woocommerce-settings-tab-demo' ),
                        'type' => 'url',
                        'desc' => __( '', 'woocommerce-settings-tab-demo' ),
                        'id'   => 'wc_settings_pos_tab_url',
                        'placeholder'=>__( 'http://example.com ', 'woocommerce-settings-tab-demo' )
                    ),
                    'username' => array(
                        'name' => __( 'Auth Username', 'woocommerce-settings-tab-demo' ),
                        'type' => 'text',
                        'desc' => __( '', 'woocommerce-settings-tab-demo' ),
                        'id'   => 'wc_settings_pos_tab_username'
                    ),
                    'password' => array(
                        'name' => __( 'Auth Password', 'woocommerce-settings-tab-demo' ),
                        'type' => 'password',
                        'desc' => __( '', 'woocommerce-settings-tab-demo' ),
                        'id'   => 'wc_settings_pos_tab_password'
                    ),
                    'Posiboltusername' => array(
                        'name' => __( 'PosiBolt Username', 'woocommerce-settings-tab-demo' ),
                        'type' => 'text',
                        'desc' => __( '', 'woocommerce-settings-tab-demo' ),
                        'id'   => 'wc_settings_pos_tab_posuser'
                    ),
                    'Posiboltpassword' => array(
                        'name' => __( 'PosiBolt Password', 'woocommerce-settings-tab-demo' ),
                        'type' => 'password',
                        'desc' => __( '', 'woocommerce-settings-tab-demo' ),
                        'id'   => 'wc_settings_pos_tab_pospassword'
                    ),
                    'terminal' => array(
                        'name' => __( 'Terminal', 'woocommerce-settings-tab-demo' ),
                        'type' => 'text',
                        'desc' => __( '', 'woocommerce-settings-tab-demo' ),
                        'id'   => 'wc_settings_pos_tab_terminal'
                    ),
                    'section_end' => array(
                         'type' => 'sectionend',
                         'id' => 'wc_settings_pos_tab_section_end'
                    )
                );
            }

            if ( 'second' == $current_section ) {
                /**
                * Filter Plugin Section 2 Settings
                */
                //This will show error for details are wrong!
                include_once( dirname( __FILE__ ) . '/get_token.php' );
                $token_id = token_genrate();
                if (!get_option('wc_settings_pos_tab_url')==NULL) {
                    if ($token_id==NULL) {
                        echo "<div class='notice notice-error is-dismissible'>
                                <p>POS Details are wrong please check it again.</p>
                              </div>";
                    }else{
                        echo "<div class='notice notice-success is-dismissible'>
                                <p>You are connected with POS.</p>
                              </div>";
                    }
                }

                $gif = dirname(plugin_dir_url( __FILE__ ));
                $gif_link =  $gif.'/loadingdata.gif';
?>
            <h2>Set Sync Schedule</h2>
            <div id="cron_data">
            </div>
            <form method="post" id="cron_form">
                <table>
                    <tr>
                        <td>Select Cron for : </td>
                        <td><input type="radio" value="DataSync" name="SyncType" id="SyncType" required="">Data Sync</td>
                        <td><input type="radio" value="ImageSync" name="SyncType" id="SyncType" required="">Image Sync</td>
                    </tr>
                    <tr>
                        <td><label">Daily</label></td>
                        <td><input type="radio" name="schedule_type" id="schedule_type" required value="daily"></td>
                        <td><input type="time" name="time_daily" id="time_daily" class="removeFocus"></td>
                    </tr>
                    <tr>
                        <td><label>Twice Daily</label></td>
                        <td><input type="radio" name="schedule_type" id="schedule_type" required value="twicedaily" /></td>
                        <td><input type="time" name="time_one" id="time_one" class="removeFocus"></td>
                        <td><input type="time" name="time_two" id="time_two" class="removeFocus"></td>
                        <td>NOTE : Cron Run Every Twelve Hour Per Day</td>
                    </tr>
                    <tr>
                        <td><label>Weekly</label></td>
                        <td><input type="radio" name="schedule_type" id="schedule_type" required value="weekly" /></td>
                        <td><select name="day" id="day_weekly">
                            <option value="sunday">Sunday</option>
                            <option value="monday">Monday</option>
                            <option value="tuesday">Tuesday</option>
                            <option value="wednesday">Wednesday</option>
                            <option value="thursday">Thursday</option>
                            <option value="friday">Friday</option>
                            <option value="saturday">Saturday</option>
                        </select></td>
                        <td><input type="time" name="time_one" id="time_weekly" class="removeFocus"></td>
                    </tr>
                    <tr>
                        <td><label>Monthly</label></td>
                        <td><input type="radio" name="schedule_type" id="schedule_type" required value="monthly" /></td>
                        <td><select name="day" id="day_monthly">
                            <option value="sunday">Sunday</option>
                            <option value="monday">Monday</option>
                            <option value="tuesday">Tuesday</option>
                            <option value="wednesday">Wednesday</option>
                            <option value="thursday">Thursday</option>
                            <option value="friday">Friday</option>
                            <option value="saturday">Saturday</option>
                        </select></td>
                        <td><input type="time" name="time_one" id="time_monthly" class="removeFocus"></td>
                        <!-- <td>NOTE : Cron will be Run next at 12:00 AM as you selected</td> -->

                    </tr>
                    <tr>
                        <td><input type="submit" id="" name="sync_Schedule" value="Set Schedule" class="btn-success btn ajax_cron_btn" data-loading-text="Please Wait..."></td>
                    </tr>
                    <?php
                        $crons  = _get_cron_array();
                        $events = array();
                        foreach ( $crons as $time => $cron ) {
                          foreach ( $cron as $hook => $dings ) {
                            foreach ( $dings as $sig => $data ) {
                              # This is a prime candidate for a Crontrol_Event class but I'm not bothering currently.
                                $events[ "$hook" ] = (object) array(
                                  'hook'     => $hook,
                                  'time'     => $time,
                                  'sig'      => $sig,
                                  'args'     => $data['args'],
                                  'schedule' => $data['schedule'],
                                  'interval' => isset( $data['interval'] ) ? $data['interval'] : null,
                                );
                              }
                            }
                          }
                        function cron_job_delete($cron_type){
                            wp_clear_scheduled_hook($cron_type);
                        }
                    ?>
                    <tr>
                      <td colspan="4">
                        <?php
                          echo "<hr>";
                          echo "<h2>Data Sync Current Schedule</h2>";

                          if(!empty($events)){
                              if (isset($events["POS_data_daily"]) && $events["POS_data_daily"]->hook=="POS_data_daily") {
                                echo "<div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA ', $events["POS_data_daily"]->time)." Sync type is <b>Daily</b></div>";
                              }
                              elseif (isset($events["POS_data_twicedaily_1"]) && $events["POS_data_twicedaily_1"]->hook=="POS_data_twicedaily_1") {
                                echo "<div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA', $events["POS_data_twicedaily_1"]->time)." Sync type is <b>Twice Daily</b></div>";
                                if (isset($events["POS_data_twicedaily_2"]) && $events["POS_data_twicedaily_2"]->hook=="POS_data_twicedaily_2") {
                                    echo "<br><div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA', $events["POS_data_twicedaily_2"]->time)." Sync type is <b>Twice Daily</b></div>";
                                }
                              }
                              elseif (isset($events["POS_data_weekly"]) && $events["POS_data_weekly"]->hook=="POS_data_weekly") {
                                echo "<div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA', $events["POS_data_weekly"]->time)." Sync type is <b>Weekly</b></div>";
                              }
                              elseif (isset($events["POS_data_monthly"]) && $events["POS_data_monthly"]->hook=="POS_data_monthly") {
                                echo "<div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA', $events["POS_data_monthly"]->time)." Sync type is <b>Monthly</b></div>";
                              }
                              else{
                                echo "<b style='color:red;'>NO Sync Schedule Yet.</b>";
                              }
                            }
                         ?>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="4">
                        <?php
                          echo "<hr>";
                          echo "<h2>Images Sync Current Schedule</h2>";

                          if(!empty($events)){
                              if (isset($events["POS_Image_daily"]) && $events["POS_Image_daily"]->hook=="POS_Image_daily") {
                                echo "<div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA ', $events["POS_Image_daily"]->time)." Sync type is <b>Daily</b></div>";
                              }
                              elseif (isset($events["POS_Image_twicedaily_1"]) && $events["POS_Image_twicedaily_1"]->hook=="POS_Image_twicedaily_1") {
                                echo "<div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA', $events["POS_Image_twicedaily_1"]->time)." Sync type is <b>Twice Daily</b></div>";
                                if (isset($events["POS_Image_twicedaily_2"]) && $events["POS_Image_twicedaily_2"]->hook=="POS_Image_twicedaily_2") {
                                    echo "<br><div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA', $events["POS_Image_twicedaily_2"]->time)." Sync type is <b>Twice Daily</b></div>";
                                }
                              }
                              elseif (isset($events["POS_Image_weekly"]) && $events["POS_Image_weekly"]->hook=="POS_Image_weekly") {
                                echo "<div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA', $events["POS_Image_weekly"]->time)." Sync type is <b>Weekly</b></div>";
                              }
                              elseif (isset($events["POS_Image_monthly"]) && $events["POS_Image_monthly"]->hook=="POS_Image_monthly") {
                                echo "<div class='cronData'>Sync Schedule at Time  ".date('d-M-Y h:iA', $events["POS_Image_monthly"]->time)." Sync type is <b>Monthly</b></div>";
                              }
                              else{
                                echo "<b style='color:red;'>NO Sync Schedule Yet.</b>";
                              }
                            }
                         ?>
                      </td>
                    </tr>
                </table>
            </form>
                <hr>
            <h2>Manually Sync</h2>
            <form id="myForm" method="post">
                <div class="bs-example">
                    <button type="button" id="sync_dis" class="btn btn-success ajaxsync syncajax" data-loading-text="Loading...">Sync POS Data</button>
                    <button type="button" id="sync_img" class="btn btn-success syncproimg" data-loading-text="Loading...">Sync Product Images</button>
                </div>
                <div class="Load" id="Load" style="display:none;">
                    <p>Please do not refresh this window or click the Back button on your browser.</p>
                    <img src="<?php echo $gif_link ?>" height="40px" width="40px"><br>
                </div>
            </form>
                <div id="results">
                    <!-- All data will display here  -->
                </div>
                <div class="imgResults" id="imgResults">
                    <h2 id="status" style="color: #5cb85c;"></h2>
                    <h2 id="error_status" style="color: red;"></h2>
                    <div id="progress_bar_back" style="display:none;">
                        <div id="Progress_Status" class="progress" style=""> 
                            <div id="myprogressBar" class="progress-bar progress-bar-striped active" >0%</div> 
                            <!-- role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" -->
                        </div>
                    </div>
                </div>
                <p id="testmsg" style="display: none;">
                    <!-- Sync Status -->
                    <span id="add_here"></span>
                </p>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<link rel="stylesheet" href="<?php echo $gif; ?>/css/progressbar.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(function() {
        jQuery(".ajaxsync").click(function()
        {
            jQuery("#sync_dis").prop("disabled", true);
            jQuery("#sync_img").hide();
            var loadingText = jQuery('#sync_dis').data('loading-text');
            //console.log(loadingText);
            jQuery('#sync_dis').html(loadingText);
            jQuery('#Load').show();
            SubmitFormData();
            jQuery("#sync_dis").prop("disabled", false);
            jQuery('#sync_dis').button('reset');
            jQuery('.ajaxsync').attr('style', 'display:none');
        });
    });
    function SubmitFormData()
    {
        jQuery.post("<?php echo plugin_dir_url( __FILE__ ).'map_products.php' ?>",function(data)
        {
            jQuery('#results').html(data);
            // jQuery('#mainform')[0].reset();
            jQuery('#Load').hide();

            jQuery("#sync_dis").prop("disabled", false);
            jQuery('#sync_dis').button('reset');
            jQuery('.ajaxsync').attr('style', 'display:none');
        });
    }

    jQuery(".syncproimg").click(function()
    {
        jQuery("#sync_dis").prop("disabled", true);
        jQuery("#sync_img").prop("disabled", true);
        var loadingText = jQuery('#sync_img').data('loading-text');
        //console.log(loadingText);
        jQuery('#sync_img').html(loadingText);
        jQuery('#Load').show();
        jQuery('#testmsg').show();
        // jQuery('#testmsg').children('span').append('Checking WooCommerce Have POS products.');
        jQuery('#progress_bar_back').show();

        jQuery.ajax({
            type: "POST",
            dataType: "json",
            url: "<?php echo plugin_dir_url( __FILE__ ).'get_woocommecre_products.php'; ?>",
            success: function(data) {
                // jQuery('#testmsg').children('span').append('<br>Getting Products Images.');
                jQuery("#sync_dis").hide();
                jQuery('#progress_bar_back').show();

                var element = document.getElementById("myprogressBar");
                
                var width = 0;
                // var identity = setInterval(scene, 40);
                var setInterval = 100/data.length;
                function scene() { 
                    if (width >= 100) {
                        clearInterval(identity); 
                    } else {
                        width = Math.round(width+setInterval);
                        if (width>100) {
                            width = 100;
                        }
                        element.style.width = width + '%';
                        // jQuery("#myprogressBar").css("width", width+'%' );
                        element.innerHTML = width * 1 + '%'; 
                    } 
                }
                var count=0;
                function getProImg(){
                    var productId = data[count]['productId'];
                    var productType = data[count]['productType'];
                    count++;
                    jQuery.ajax({
                        async: true,
                        type: "POST",
                        data: {id:productId, proType:productType,length:data.length,count:count},
                        url: "<?php echo plugin_dir_url( __FILE__ ).'get_product_image.php'; ?>",
                        success: function(imgdata) {
                            scene();
                            // console.log(imgdata);
                            // jQuery('#testmsg').children('span').append(imgdata);
                            // return false;
                            if (count< data.length) {
                                getProImg();
                            }else{
                                jQuery('#status').append('Sync Complete');
                                jQuery('#Load').hide();
                                jQuery('#sync_img').hide();
                                jQuery("#sync_img").prop("disabled", false);
                                jQuery('#sync_img').button('reset');
                                // jQuery('#progress_bar_back').hide();
                                // jQuery('#Load').hide(); 
                            }
                        },
                        error:function(imgdata){
                            // console.log(imgdata + ' getting some issue.');
                            jQuery('#error_status').append('something Wrong please check');
                        }
                    });
                }
                
                getProImg(); // Call function for execution.
            },
            error: function(data){
                // console.log('Error : '+imgdata);
                jQuery('#error_status').append('something Wrong please check');
            }
        });
    });

    function fieldBlankFocus(value){
        jQuery(".removeFocus").css('border','');
        jQuery("#"+value).focus();
        jQuery("#"+value).css('border','solid 1px red');
        jQuery("#"+value).change(function(){
            jQuery("#"+value).css('border','');
        });
    }

    jQuery("form").on("submit", function (e) {
        e.preventDefault();
        var data =[];
        var sync_type = jQuery("#SyncType:checked").val();
        var schedule_type = jQuery("#schedule_type:checked").val();

        if (sync_type) {
            data.push(["sync_type" , sync_type]);
        }
        switch(schedule_type){
            case 'daily':
                var time_daily = jQuery("#time_daily").val();
                if (time_daily == '') {
                    fieldBlankFocus("time_daily");
                }
                data.push("daily" , time_daily);
            break;
            case 'twicedaily':
                var time_one = jQuery("#time_one").val();
                if (time_one == '') {
                    fieldBlankFocus("time_one");
                    return false;
                }
                var time_two = jQuery("#time_two").val();
                if (time_two == '') {
                    fieldBlankFocus("time_two");
                    return false;
                }
                data.push("twicedaily", time_one ,time_two);
            break;
            case 'weekly':
                var day_monthly = jQuery("#day_weekly").val();
                var time_weekly = jQuery("#time_weekly").val();
                if (time_weekly == '') {
                    fieldBlankFocus("time_weekly");
                }
                data.push("weekly", day_monthly ,time_weekly);
            break;
            case 'monthly':
                var day_monthly = jQuery("#day_monthly").val();
                var time_monthly = jQuery("#time_monthly").val();
                if (time_monthly == '') {
                    fieldBlankFocus("time_monthly");
                }
                data.push("monthly", day_monthly ,time_monthly);
            break;
        }
        if (data[2] == '' || data[3] == '') {
            return false;
        }
        console.log(data);
        jQuery(".ajax_cron_btn").prop("disabled", true);

            jQuery.post("<?php echo plugin_dir_url( __FILE__ ).'/pos_cron_setup.php'; ?>", { data },
               function(data) { 
                // console.log(data);
                setTimeout(function() {
                   jQuery(".ajax_cron_btn").prop("disabled", false);
                }, 600000);

                 jQuery('#cron_data').html(data);
                 jQuery('#cron_form')[0] && jQuery('#cron_form')[0].reset();
            });
    });
</script>
<style type="text/css">
    .btn-success {
        color: #fff;
        background-color: #3a7aab;
        border-color: #4cae4c;
    }
    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    .cronData{
        background: #467ca6;
        padding: 8px;
        color: white;
    }
    .wp-core-ui .button-primary{display: none;}
</style>

<?php
            }

            if ( 'third' == $current_section ) {
                echo "<div class='logDiv'>";
                $logFile = dirname(plugin_dir_path( __FILE__ )).'/logs/php_error.log';
                if (file_exists($logFile)) {
                    $file = file($logFile);
                    $file = array_reverse($file);
                    foreach($file as $f){
                        echo $f."<br />";
                    }
                }else{
                    echo "<div class='notice notice-warning is-dismissible'>
                            <p>POS Log File Is Not Exist </p>
                          </div>";
                }
                
                
                echo "</div>";
?>  
                <style>
                    .logDiv{
                        position: absolute;
                        background-color: #fff;
                        overflow: scroll;
                        font-weight: 900;
                        height: 400px;
                        width: 98%;  
                    }
                    .wp-core-ui .button-primary{display: none;}
                </style>
<?php
            }
            /**
            * Filter MyPlugin Settings
            */
            if(!empty($settings)){
            return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section );
            }
        }
        /**
         * Output the settings
         */
        public function output() {

        global $current_section;

        $settings = $this->get_settings( $current_section );
        WC_Admin_Settings::output_fields( $settings );
        }
        /**
          * Save settings
         */
        public function save() {

         global $current_section;

         $settings = $this->get_settings( $current_section );
         WC_Admin_Settings::save_fields( $settings );

        }
    }
    return new WC_Settings_pos();
}